// var gulp = require('gulp'),
//     // newer = require('gulp-newer'),
//     font64 = require('gulp-simplefont64'),
//     cssfont64 = require('gulp-cssfont64'),
//     browserSync = require('browser-sync'),
//     reload = browserSync.reload;
//
// // gulp.task('fonts', function() {
// //     gulp.src('./assets/fonts/**/*')
// //         .pipe(newer('./public/fonts'))
// //         .pipe(gulp.dest('./public/fonts'))
// //         .pipe(reload({
// //             stream: true,
// //         }));
//
// // });
//
// gulp.task('fonts', function() {
//     return gulp.src(['./assets/fonts/**/*.woff','./assets/fonts/**/*.woff2'])
//         .pipe(cssfont64())
//         .pipe(gulp.dest('./public/css/'))
//         .pipe(reload({
//             stream: true,
//         }));
// });

var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    prefix = require('gulp-autoprefixer'),
    replace = require('gulp-replace'),
    browserSync = require('browser-sync'),
    cssBase64 = require('gulp-css-base64'),
    reload = browserSync.reload;



gulp.task('fonts', function() {
    gulp.src('./assets/fonts/**/*.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(cssBase64({
            baseDir: "../../assets/fonts",
            maxWeightResource: 1000000
        }))
        .pipe(prefix({
            browsers: ['last 2 versions', 'IE 9', 'IE 10']
        }))
        .pipe(replace(/font-woff/g, 'x-font-woff'))
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        }));
});
