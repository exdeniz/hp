var gulp = require('gulp'),
    express = require("express"),
    fs = require('fs'),
    favicon = require('serve-favicon'),
    compression = require('compression'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;


gulp.task('express', function() {
    var app = express();
    app.set('views', './assets/template');
    app.use(favicon('./public/favicon.ico'));
    app.set('view engine', 'jade');

    app.use(compression({
        threshold: 512
    }));
    app.use(express.static('./public'));
    app.listen(9001);
    app.get('/', function(req, res) {
        res.render('index');
    });
    app.get('/:file', function(req, res) {
        res.render(req.params.file);
    });
    console.log('Listening on port: 9001');
});

gulp.task('express-pagespeed', function() {
    var app = express();
    // app.set('views', __dirname + './assets/template');
    // app.set('view engine', 'jade');
    app.use(compression({
        threshold: 512
    }));
    app.use(express.static(__dirname + './build'));
    app.listen(9001);
    app.get('/', function(req, res) {
        res.render('index');
    });
    app.get('/:file', function(req, res) {
        res.render(req.params.file);
    });
    console.log('Listening on port: 9001');
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "localhost:9001",
        //tunnel: true,
        browser: "google chrome",
        startPath: "/index"
    });
});
