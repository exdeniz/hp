$(".js-show-news").on("click", function() {
    $(".newsOverlay").toggleClass('newsOverlayShow')
    $("html").toggleClass("has-open-news");
    return false;
})
$(".js-hide-news").on("click", function() {
    newsHide();
    return false;
})

var body = $("body");


body.swipe({swipeUp:newsShow, allowPageScroll:"vertical"})
$('.news').swipe({swipeDown:newsHide, allowPageScroll:"vertical",threshold:150})

function newsShow(event, direction, distance, duration, fingerCount) {
    $(".newsOverlay").addClass('newsOverlayShow')
    $(".js-up-news").addClass('newsButtonUpShow');
    $("html").addClass("has-open-news");
}
function newsHide(event, direction, distance, duration, fingerCount) {
    $("html").removeClass("has-open-news");
    $('.news').scrollTo(0,0)
    $(".newsOverlay").removeClass('newsOverlayShow')
    $(".js-up-news").removeClass('newsButtonUpShow');
}


docReady(function() {

    var grid = document.querySelector('.newsItems');
    var msnry = new Masonry(grid, {
         columnWidth: '.newsItem',
         itemSelector: '.newsItem',
         "gutter": '.newsItemGutter',
         isFitWidth: true
    });



    jQuery(function($) {
        $('.news').bind('scroll', function() {
            if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
                    var elems = [];
                    var fragment = document.createDocumentFragment();
                    for (var i = 0; i < 3; i++) {
                        var elem = getItemElement();
                        fragment.appendChild(elem);
                        elems.push(elem);
                    }
                    // append elements to container
                    grid.appendChild(fragment);
                    // add and lay out newly appended elements
                    msnry.appended(elems);
            }
        })
    });

});

// create <div class="grid-item"></div>
function getItemElement() {
    var elem = document.createElement('div');
    elem.className = 'newsItem';
    elem.innerHTML = '<div class="newsItemImage"><img src="img/newsImage-1.jpg" alt=""></div><div class="newsItemText">#hundp #agentur</div><div class="newsItemIconFB"></div>'
    return elem;
}

$(window).bind('DOMMouseScroll mousewheel', function(event) {
    var delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
    if (delta >= 0) {
        if($('.news').scrollTop() == 0) {
            newsHide();
        }
    }
    else {
        newsShow();
    }
});

$('.js-up-news').click(function () {
    $('.news').scrollTo(0,500, { easing:'swing' } );
    newsHide();
})

$('.bannerItems').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    lazyLoad: 'progressive',
    mobileFirst: true
});

$(".js-show-nav").on("click", function() {
    $("html").toggleClass("has-open-nav");
    return false;
});


var timer;

$(window).on('mousemove', function() {
    var hovered = $(".slick-next:hover, .slick-prev:hover").length;

    $('.slick-next').addClass('slick-arrowShow');
    $('.slick-prev').addClass('slick-arrowShow');
    try {
        clearTimeout(timer);
    } catch (e) {}
    // $(".slick-next, .slick-prev").mouseout(function() {
    if (!hovered) {
        timer = setTimeout(function() {
            $('.slick-next').removeClass('slick-arrowShow');
            $('.slick-prev').removeClass('slick-arrowShow');
        }, 500);
    }

});
$('.bannerMarkenItems').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false
});


$(function(){
    if (location.hash.indexOf('slide') + 1) {
        var slide = Number(location.hash.replace(/\D+/g,""));
        $('.bannerMarkenItems').slick('slickGoTo', slide - 1);
        $('.bannerItems').slick('slickGoTo', slide - 1);    }
});

window.onhashchange = function () {
    if (location.hash.indexOf('slide') + 1) {
        var slide = Number(location.hash.replace(/\D+/g,""));
        $('.bannerMarkenItems').slick('slickGoTo', slide - 1);
        $('.bannerItems').slick('slickGoTo', slide - 1);    }
}

$('.videoSlide').vide({
  mp4: './video/westpark_spot_v3_website_1.mp4',
  ogv: './video/westpark_spot_v3_website_1.ogv',
  webm: './video/westpark_spot_v3_website_1.webm'
});

// $(window).on('orientationchange', function(e) {
//      window.location.reload();
// });

viewportUnitsBuggyfill.init();
