/// <reference path='../lib/app.ts' />
function MessageGroup(firstMessages:any) {
    //static $inject = ['$timeout'];
    return function(messages: IHFMessage[]): any {
        if (!messages) return [];
        firstMessages = firstMessages || {};
        var groups = _.groupBy(messages, function(message) {
            return moment().date(message.createdAt.getDate()).month(message.createdAt.getMonth()).year(message.createdAt.getFullYear());
        });
        _.map(groups, function(group, date){
            var firstMessage = _.first(group);
            firstMessages[firstMessage._id] = date;
        });
        return messages;
        //return _.flatten(_.pairs(groups))
    }
}
angular.module('hf-client').filter('messagesGroup', [MessageGroup]);