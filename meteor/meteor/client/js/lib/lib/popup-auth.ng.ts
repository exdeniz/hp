/// <reference path='../../../../typings/angular-meteor/angular-meteor.d.ts' />
/*
** Наработка компании ООО Умные Системы
** По вопросам поддержки и сотрудничества обращаться к Баринову Роману
** rbarinov@gmail.com
** +7 (926) 7372853
*/

declare var Url: any;
declare var chrome: any;
angular.module('barinov.popup', [])
	.provider('nbpopup', [function() {
		var config = this;

		this.$get = [
			'$rootScope',
			'$q',
			'$injector',
			'$window',
			'$location',
			'$timeout',
			function($rootScope, $q, $injector, $window, $location, $timeout) {
				var nbpopup = {
					popup: function(url, width, height) {
						var defer, callback, opts, frm, getMessage, gotmessage, interval, res, wnd, wndTimeout, wnd_options, wnd_settings;
						gotmessage = false;
						getMessage = function(e) {
							if (e && e.data && e.data.indexOf && e.data.indexOf('Meteor') === 0) {
								return;
							}
							defer.resolve(angular.fromJson(e.data));

							if (!gotmessage) {
								try {
									wnd.close();
								} catch (_error) { }
								opts.data = e.data;
								return gotmessage = true;
							}
						};
						wnd = void 0;
						frm = void 0;
						wndTimeout = void 0;
						defer = $q.defer();
						opts = opts || {};
						if (arguments.length === 2 && typeof opts === 'function') {
							callback = opts;
							opts = {};
						}
						wnd_settings = {
							width: Math.floor(window.outerWidth * 0.8),
							height: Math.floor(window.outerHeight * 0.5)
						};

						if (width) {
							wnd_settings.width = width;
						}
						if (height) {
							wnd_settings.height = height;
						}

						if (wnd_settings.height == null) {
							wnd_settings.height = (wnd_settings.height < 350 ? 350 : void 0);
						}
						if (wnd_settings.width == null) {
							wnd_settings.width = (wnd_settings.width < 800 ? 800 : void 0);
						}
						if (wnd_settings.left == null) {
							wnd_settings.left = window.screenX + (window.outerWidth - wnd_settings.width) / 2;
						}
						if (wnd_settings.top == null) {
							wnd_settings.top = window.screenY + (window.outerHeight - wnd_settings.height) / 8;
						}
						wnd_options = "width=" + wnd_settings.width + ",height=" + wnd_settings.height;
						wnd_options += ",toolbar=0,scrollbars=1,status=1,resizable=1,location=1,menuBar=0";
						wnd_options += ",left=" + wnd_settings.left + ",top=" + wnd_settings.top;
						// opts = {
						//   provider: provider,
						//   cache: opts.cache
						// };
						opts.callback = function(e, r) {
							if (window.removeEventListener) {
								window.removeEventListener("message", getMessage, false);
							} else if (window['detachEvent']) {
								window['detachEvent']("onmessage", getMessage);
							} else {
								if (document['detachEvent']) {
									document['detachEvent']("onmessage", getMessage);
								}
							}
							opts.callback = function() { };
							if (wndTimeout) {
								clearTimeout(wndTimeout);
								wndTimeout = undefined;
							}
							if (callback) {
								return callback(e, r);
							} else {
								return undefined;
							}
						};
						if (window['attachEvent']) {
							window['attachEvent']("onmessage", getMessage);
						} else if (document['attachEvent']) {
							document['attachEvent']("onmessage", getMessage);
						} else {
							if (window.addEventListener) {
								window.addEventListener("message", getMessage, false);
							}
						}
						if (typeof chrome !== "undefined" && chrome.runtime && chrome.runtime.onMessageExternal) {
							chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
								request.origin = sender.url.match(/^.{2,5}:\/\/[^/]+/)[0];
								return getMessage(request);
							});
						}
						if (!frm && (navigator.userAgent.indexOf("MSIE") !== -1 || navigator.appVersion.indexOf("Trident/") > 0)) {

							frm = document.createElement("iframe");
							frm.src = config.oauthd_url + "/auth/iframe?d=" + encodeURIComponent(Url.getAbsUrl("/"));
							frm.width = 0;
							frm.height = 0;
							frm.frameBorder = 0;
							frm.style.visibility = "hidden";
							document.body.appendChild(frm);
						}
						wndTimeout = setTimeout(function() {
							if (defer != null) {
								defer.reject({ error: "Authorization timed out" });
							}
							if (opts.callback && typeof opts.callback === "function") {
								opts.callback(new Error("Authorization timed out"));
							}
							try {
								wnd.close();
							} catch (_error) { }
						}, 1200 * 1000);
						wnd = window.open(url, "Authorization", wnd_options);
						if (wnd) {
							wnd.focus();
							interval = window.setInterval(function() {
								if (wnd === null || wnd.closed) {
									window.clearInterval(interval);
									if (!gotmessage) {
										if (defer != null) {
											defer.reject({ error: "The popup was closed" });
										}
										if (opts.callback && typeof opts.callback === "function") {
											return opts.callback(new Error("The popup was closed"));
										}
									}
								}
							}, 500);
						} else {
							if (defer != null) {
								defer.reject({ error: "Could not open a popup" });
							}
							if (opts.callback && typeof opts.callback === "function") {
								opts.callback(new Error("Could not open a popup"));
							}
						}

						return defer.promise.then(function(data) {
							wnd.close();
							return data
						})
					}
				}
				return nbpopup;
			}
		];
	}
	]);