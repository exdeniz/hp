/// <reference path='../../../../typings/meteor/meteor.d.ts' />
// module Meteor {
// export var loginWithBuvle = function(accessToken: string, callback: (error?: any) => void) {

// declare var Accounts: any;
Meteor['loginWithBuvle'] = function(accessToken: string, callback: (error?: any) => void): any {
    var buvleAuthOptions = {
        access_token: accessToken,
        isBuvle: true
    };
    Accounts['callLoginMethod']({
        methodArguments: [buvleAuthOptions],
        userCallback: function(error, result) {
            if (error) {
                callback && callback(error);
            } else {
                callback && callback();
            }
        }
    });
};
// }