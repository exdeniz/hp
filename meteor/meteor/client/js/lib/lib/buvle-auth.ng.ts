/// <reference path='../../../../typings/angular-meteor/angular-meteor.d.ts' />
/// <reference path='angular-jwt.ng.ts' />
/// <reference path='angular-local-storage.ng.ts' />
/// <reference path='popup-auth.ng.ts' />

/*
** Наработка компании ООО Умные Системы
** По вопросам поддержки и сотрудничества обращаться к Баринову Роману
** rbarinov@gmail.com
** +7 (926) 7372853
*/
angular.module('barinov.auth', ['LocalStorageModule', 'angular-jwt', 'barinov.popup'])
    .provider('auth', [
        'jwtInterceptorProvider',
        '$httpProvider',
        function(jwtInterceptorProvider,
            $httpProvider) {
            var config = this;

            var refreshTokenTimeout = 30 * 60 * 1000;

            config.oauth = {};

            jwtInterceptorProvider.tokenGetter = ['auth', 'jwtHelper', function(auth, jwtHelper) {
                if (auth.accessToken) {
                    if (!jwtHelper.isTokenExpired(auth.accessToken)) {
                        return auth.accessToken;
                    } else {
                        var promise = auth.refreshPromise = auth.refreshPromise || auth.refresh(auth.refreshToken);

                        return promise
                            .then(function(data) {
                                return data.access_token
                            }, function() {
                                auth.signout();
                                // Meteor.logout();
                            })
                            .finally(function() {
                                auth.refreshPromise = undefined
                            })
                    }
                }

                return ''
            }];
            $httpProvider.interceptors.push('jwtInterceptor');

            this.$get = [
                '$rootScope',
                '$q',
                '$injector',
                '$window',
                '$location',
                '$http',
                'localStorageService',
                'jwtHelper',
                '$timeout',
                'nbpopup',
                function($rootScope: angular.meteor.IRootScopeService, $q: angular.IQService, $injector, $window, $location, $http, localStorageService, jwtHelper, $timeout: angular.ITimeoutService, nbpopup) {

                    var auth: any = { isAuthenticated: false };
                    auth.config = config;

                    auth.oauth = {
                        popup: function(provider, opts, callback) {
                            var url = config.oauth.prefix + "auth/" + provider;

                            if (window['cordova']) {
                                var dfd = $q.defer();

                                var browserRef = window.open(url);

                                browserRef.addEventListener('loadstart', function(event) {
                                    if ((event.url).indexOf("http://localhost/callback") === 0) {
                                        var callbackResponse = (event.url).split("#")[1];
                                        var responseParameters = (callbackResponse).split("&");
                                        var parameterMap: any = [];
                                        for (var i = 0; i < responseParameters.length; i++) {
                                            parameterMap[responseParameters[i].split("=")[0]] = responseParameters[i].split("=")[1];
                                        }
                                        if (parameterMap.access_token !== undefined && parameterMap.access_token !== null) {
                                            dfd.resolve(parameterMap);
                                        } else {
                                            dfd.reject("Problem authenticating");
                                        }
                                        setTimeout(function() {
                                            window.close();
                                            // dfd.close();
                                        }, 10);
                                    }
                                });

                                browserRef.addEventListener('exit', function(event) {
                                    dfd.reject("The sign in flow was canceled");
                                });

                                return dfd.promise;
                            } else {
                                return nbpopup.popup(url)
                                    .then(function(data) {
                                        return data
                                    })
                            }
                        },
                        me: function(oauthResult) {
                            var dfd = $q.defer();

                            $http.post(auth.config.oauth.prefix + 'me', oauthResult)
                                .success(function(me) {
                                    dfd.resolve(me)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise
                        }
                    }

                    auth.user = {};
                    auth.user = {
                        addProvider: function(options) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/addProvider', options)
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise

                        },
                        removeProvider: function(options) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/removeProvider', options)
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise

                        },
                        getProviders: function() {
                            var dfd = $q.defer()

                            $http.get(auth.config.oauth.prefix + 'user/getProviders')
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise

                        },
                        getProvider: function(provider) {
                            var dfd = $q.defer()

                            $http.get(auth.config.oauth.prefix + 'user/getProvider', { params: { provider: provider } })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise

                        },
                        signup: function(options) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/signup', options)
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                });

                            return dfd.promise
                        },
                        changeEmail: function(id, email) {
                            var dfd = $q.defer();

                            $http.post(auth.config.oauth.prefix + 'user/changeEmail',
                                { id: id, email: email })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                });

                            return dfd.promise
                        },
                        changePassword: function(password) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/changePassword', { password: password })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise
                        },
                        changeUserpic: function(userpic) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/changeUserpic', { userpic: userpic })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise
                        },
                        changeProfile: function(profile) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/changeProfile', profile)
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise
                        },
                        restore: function(email) {
                            var dfd = $q.defer()

                            $http.post(auth.config.oauth.prefix + 'user/tryRestore', {}, { params: { email: email } })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                })

                            return dfd.promise
                        },
                        reset: function(options) {
                            var dfd = $q.defer();

                            $http.post(auth.config.oauth.prefix + 'user/resetPassword', {
                                login: options.login,
                                token: options.token,
                                password: options.password
                            })
                                .success(function(data) {
                                    dfd.resolve(data)
                                })
                                .error(function(error) {
                                    dfd.reject(error)
                                });

                            return dfd.promise;
                        },
                        resendActivation: function() {
                            var dfd = $q.defer();
                            $http.post(auth.config.oauth.prefix + 'user/resendActivation', null)
                                .success(function() {
                                    dfd.resolve();
                                })
                                .error(function(d, c) {
                                    if (c == 401) {
                                        dfd.reject('unauthorized');
                                    }
                                    if (c == 400) {
                                        dfd.reject('user not found');
                                    }
                                });
                            return dfd.promise;
                        }
                    };

                    auth.reloadTokensFromStorage = function() {
                        var token = localStorageService.get(config.oauth.accessTokenPath);
                        var refresh_token = localStorageService.get(config.oauth.refreshTokenPath);
                        if (token) {
                            if (!jwtHelper.isTokenExpired(token)) {
                                auth.onAuthenticated(token, refresh_token, {});
                            } else if (refresh_token) {
                                auth.onAuthenticated(token, refresh_token, {})
                            } else {
                                $location.path('/');
                            }
                        }
                    }

                    $rootScope.$on('$locationChangeStart', function() {
                        if (!auth.isAuthenticated) {
                            Meteor.logout();
                            auth.reloadTokensFromStorage()
                        }
                    });

                    var refreshTokenUpdatePromise: any;

                    auth.onAuthenticated = function(accessToken, refreshToken, profile) {
                        var dfd = $q.defer<any>();
                        Meteor['loginWithBuvle'](accessToken, function(err, res) {
                            if (err) {
                                console.log('login with buvle error');
                                dfd.reject(err);
                            } else {
                                console.log('login with buvle');
                                dfd.resolve();
                            }
                        });
                        var profilePromise = auth.getProfile(accessToken);
                        var response = {
                            accessToken: accessToken,
                            refreshToken: refreshToken,
                            profile: profile,
                            isAuthenticated: true
                        };

                        function doScheduleRefresh() {
                            if (refreshTokenUpdatePromise) {
                                $timeout.cancel(refreshTokenUpdatePromise);
                            }

                            auth.refresh(refreshToken);

                            refreshTokenUpdatePromise = $timeout(function() {
                                doScheduleRefresh();
                            }, refreshTokenTimeout);
                        }

                        doScheduleRefresh();

                        angular.extend(auth, response);
                        $rootScope.$broadcast('login');



                        return $q.all([profilePromise, dfd.promise]); //.then(() => { return profilePromise });
                    };

                    auth.socialLogin = function(options) {
                        var dfd = $q.defer();
                        $http.post(config.oauth.socialLoginUrl, options, { skipAuthorization: true })
                            .success(function(data) {
                                localStorageService.set(config.oauth.accessTokenPath, data.access_token);
                                if (data.refresh_token) {
                                    localStorageService.set(config.oauth.refreshTokenPath, data.refresh_token);
                                }
                                auth.onAuthenticated(data.access_token, data.refresh_token, {})
                                    .then(() => {
                                        dfd.resolve(data);
                                    }, (err) => {
                                        dfd.reject(err);
                                    })

                            }).error(dfd.reject);
                        return dfd.promise;
                    };

                    auth.passwordGrant = function(username, password) {
                        var dfd = $q.defer();

                        $http.post(config.oauth.url, { grant_type: 'password', username: username, password: password }, { skipAuthorization: true })
                            .success(function(data) {
                                localStorageService.set(config.oauth.accessTokenPath, data.access_token);

                                if (data.refresh_token) {
                                    localStorageService.set(config.oauth.refreshTokenPath, data.refresh_token);
                                }

                                auth.onAuthenticated(data.access_token, data.refresh_token, {})
                                    .then(() => {
                                        dfd.resolve(data)
                                    }, (err) => {
                                        dfd.reject(err);
                                    });

                            })
                            .error(function(error) {
                                dfd.reject(error)
                            });

                        return dfd.promise
                    };

                    auth.exitImpersonation = function() {
                        if (auth.impersonated) {
                            auth.reloadTokensFromStorage()
                            auth.impersonated = false;
                        }
                    }

                    auth.impersonate = function(username) {
                        var dfd = $q.defer()

                        $http.post(config.oauth.impersonate, { username: username }, { skipAuthorization: false })
                            .success(function(data) {
                                auth.impersonated = true
                                auth.onAuthenticated(data.access_token, data.refresh_token, {})
                                    .then(() => {
                                        dfd.resolve(data)
                                    }).catch((err) => {
                                        dfd.reject(err);
                                    });
                            })
                            .error(function(error) {
                                dfd.reject(error)
                            })

                        return dfd.promise
                    }

                    auth.refresh = function(refresh_token) {
                        var dfd = $q.defer();

                        $http.post(config.oauth.url, { grant_type: 'refresh_token', refresh_token: refresh_token }, { skipAuthorization: true })
                            .success(function(data) {
                                localStorageService.set(config.oauth.accessTokenPath, data.access_token);
                                auth.accessToken = data.access_token
                                dfd.resolve(data)
                            })
                            .error(function(error) {
                                dfd.reject(error)
                            })

                        return dfd.promise
                    }

                    auth.signout = function() {
                        Meteor.logout();
                        auth.isAuthenticated = false;
                        auth.profile = null;
                        auth.profilePromise = null;
                        auth.accessToken = null;
                        auth.refreshToken = null;

                        localStorageService.remove(config.oauth.accessTokenPath);
                        localStorageService.remove(config.oauth.refreshTokenPath);
                        if (refreshTokenUpdatePromise) {
                            $timeout.cancel(refreshTokenUpdatePromise);
                        }
                        // console.log('logout');
                    };

                    auth.getProfile = function() {
                        var dfd = $q.defer();

                        $http.get(config.oauth.me)
                            .success(function(profile) {
                                dfd.resolve(profile)
                            })
                            .error(function(error) {
                                dfd.reject(error || 'Неизваетсная ошибка')
                            });

                        return auth.profilePromise = dfd.promise
                            .then(function(profile) {
                                auth.profile = profile;
                                return profile;
                            })
                    };

                    return auth;
                }
            ];
        }
    ]);