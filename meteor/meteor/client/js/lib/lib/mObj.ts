/// <reference path='../../../../typings/tsd.d.ts'/>
interface IMObjEvalParams {
	mObjId?: string,
	subscriptionName?: string,
	collection?: Mongo.Collection<any>,
	mObjIdKey: string,
	scopeField: string,
	autoClientSave: boolean,
	onlyLocal: boolean
}
interface mObjScope extends ng.meteor.IScope {
	$root: ng.meteor.IRootScopeService;
	currentUser: Meteor.User;
	$meteorAutorun(autorunFunction: Function);
	$meteorCollectionFS<T>(collection: Mongo.Collection<T>, autoClientSave?: boolean): angular.meteor.AngularMeteorCollection<T>;
	$meteorCollectionFS<T, U>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave: boolean, updateCollection: Mongo.Collection<U>): angular.meteor.AngularMeteorCollection2<T, U>;
	$meteorCollection<T>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave?: boolean): angular.meteor.AngularMeteorCollection<T>;
	$meteorCollection<T, U>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave: boolean, updateCollection: Mongo.Collection<U>): angular.meteor.AngularMeteorCollection2<T, U>;
	$meteorObject<T>(collection: Mongo.Collection<T>, selector: Mongo.Selector | Mongo.ObjectID | string, autoClientSave?: boolean): angular.meteor.AngularMeteorObject<T>;
	$meteorSubscribe(name: string, ...publisherArguments: any[]): angular.IPromise<Meteor.SubscriptionHandle>;
	evalParams: IMObjEvalParams;
	mObjSubsHandle?: ng.IPromise<Meteor.SubscriptionHandle>;
}
angular.module('correpw_mObj', ['angular-meteor']);
angular.module('correpw_mObj').directive('mObj', ['$meteor', function($meteor: ng.meteor.IMeteorService) {
	return {
		restrict: 'A',
		scope: {
			'ngModel': '=',
			'mObjSubsHandle': '=',
			'mOnlyLocal': '=',
			'mAutoSave': '=',
			'mCollection': '='
		},
		link: function(scope: mObjScope, element: ng.IAugmentedJQuery, iAttrs: ng.IAttributes) {
			///constructor
			scope.evalParams = {
				collection: undefined,
				mObjId: undefined,
				subscriptionName: undefined,
				mObjIdKey: '_id',
				scopeField: 'ngModel',
				autoClientSave: false,
				onlyLocal: false
			};

			var _eval = (mObjId: string, subscriptionName: string, collection: Mongo.Collection<any>,
				scopeField: string, mObjIdKey: string, autoClientSave: boolean,
				onlyLocal: boolean) => {
				var selector = {}; selector[mObjIdKey] = mObjId;
				var bindObjToScope = () => {
					scope[scopeField] = scope.$meteorObject(collection, selector, autoClientSave);
					// console.log(scope[scopeField]);
					if (!scope[scopeField]._serverBackup || !scope[scopeField]._serverBackup._id) {
						delete scope[scopeField];
					}
				}
				var subscribe = () => {
					return scope.$meteorSubscribe(subscriptionName, mObjId).then((subscriptionHandle) => {
						bindObjToScope();
						return subscriptionHandle;
					});
				}
				if (!onlyLocal && collection) {
					return subscribe()
				} else {
					bindObjToScope();
				}
			}

			scope.$watch<IMObjEvalParams>('evalParams', (n, o) => {
				if (n && n.subscriptionName && n.collection && n.mObjId) {
					scope.mObjSubsHandle = _eval(n.mObjId, n.subscriptionName,
						n.collection, n.scopeField, n.mObjIdKey, n.autoClientSave, n.onlyLocal);
				} else if (o) {
					if (scope[o.scopeField]) {
						delete scope[o.scopeField];
					}
				}
			});

			iAttrs.$observe<string>('mObjId', (mObjId) => {
				scope.evalParams = angular.extend({}, scope.evalParams, { mObjId: mObjId });
			});
			iAttrs.$observe<string>('mSubscription', (subscriptionName) => {
				scope.evalParams = angular.extend({}, scope.evalParams, { subscriptionName: subscriptionName });
			});
			iAttrs.$observe<string>('mCollName', (collectionName) => {
				scope.evalParams = angular.extend({}, scope.evalParams, { collection: $meteor.getCollectionByName(collectionName) });
			});
			scope.$watch<boolean>('mOnlyLocal', (n, o) => {
				if (n != undefined && n != null) {
					scope.evalParams = angular.extend({}, scope.evalParams, { onlyLocal: n });
				}
			});
			scope.$watch<boolean>('mAutoSave', (n, o) => {
				if (n != undefined && n != null) {
					scope.evalParams = angular.extend({}, scope.evalParams, { autoClientSave: n });
				}
			});
			scope.$watch<string>('mSelectorKey', (n, o) => {
				if (n != undefined && n != null) {
					scope.evalParams = angular.extend({}, scope.evalParams, { mObjIdKey: n });
				}
			});
			scope.$watch<Mongo.Collection<any>>('mCollection', (n, o) => {
				if (n != undefined && n != null) {
					scope.evalParams = angular.extend({}, scope.evalParams, { collection: n });
				}
			});
		}
	};
}]);