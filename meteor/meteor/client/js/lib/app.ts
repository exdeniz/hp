/// <reference path='lib/buvle.oauth.angular.ts'/>
/// <reference path="../../../lib/Notification.ts" />
/// <reference path='../interfaces/Scopes.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../../../lib/EmployersCollection.ts' />
/// <reference path='../../../lib/InstitutionsCollection.ts' />
/// <reference path='../../../lib/ThemesCollection.ts' />
/// <reference path='../../../typings/tsd.d.ts' />
/// <reference path='../controllers/headerController.ts' />
/// <reference path='../controllers/signupController.ts' />
/// <reference path='../controllers/profileInfoController.ts' />
/// <reference path='../services/uiService.ts' />
/// <reference path='../services/subscriptionsService.ts' />
/// <reference path='../services/themeService.ts' />
/// <reference path='../controllers/profilePublicController.ts' />
/// <reference path='../controllers/subscriptionsBarController.ts' />
/// <reference path='../controllers/profileSubscriptionsController.ts' />
/// <reference path='../controllers/profileFollowersController.ts' />
/// <reference path='../controllers/profilePrivateController.ts' />
/// <reference path='../controllers/profileSettingsController.ts' />
/// <reference path='../controllers/imController.ts' />
/// <reference path='../controllers/imChatController.ts' />
/// <reference path='../controllers/privateFeedController.ts' />
/// <reference path='../controllers/themeController.ts' />
/// <reference path='../controllers/public.we.ts' />
/// <reference path='../directives/sidebar.left.ts' />
/// <reference path='../directives/avatar.ts' />
/// <reference path='../directives/subscribeUnsubscribe.ts' />
/// <reference path='../directives/post.add.ts' />
/// <reference path='lib/mObj.ts' />
/// <reference path='../directives/icheck.ts' />
/// <reference path='../filters/messagesGroups.ts' />

var hfclient = angular.module('hf-client', [
	'angular-meteor',
	'ui.router',
	'correpw_mObj',
	'corre_pw.auth',
	'ui.bootstrap',
	'ngFileUpload',
	'ui.select',
	'ngSanitize',
	'angularMoment',
	'ui.slimscroll',
// 'ngFx',
// 'ngAnimate',
	'angular-inview',
	'ngImgCrop',
	// 'ng-fx',
	// 'ngAnimate',
	'yaMap']);

hfclient.config([
	'$stateProvider',
	'$urlRouterProvider',
	'$logProvider',
	'$locationProvider',
	'authProvider',
	function(
		$stateProvider: angular.ui.IStateProvider,
		$urlRouterProvider: angular.ui.IUrlRouterProvider,
		$logProvider: angular.ILogProvider,
		$locationProvider: angular.ILocationProvider,
		authProvider) {
		//log
		$logProvider.debugEnabled(true);
		
		//auth
		
		var prefix = Meteor.settings['public']['OAUTH_HOST'];
		
		// if (!prefix || prefix.indexOf('http') == -1) {
		// 	prefix = "http://corre.pw/auth/";
		// }
		
		authProvider.oauth.prefix = prefix
		authProvider.oauth.url = prefix + 'oauth/token'
		authProvider.oauth.impersonate = prefix + 'oauth/impersonate';
		authProvider.oauth.me = prefix + 'profile/me'
		authProvider.oauth.accessTokenPath = 'access_token'
		authProvider.oauth.refreshTokenPath = 'refresh_token';
		authProvider.oauth.socialLoginUrl = prefix + 'oauth/socialToken';

		//location
		$locationProvider.html5Mode(true);
		$locationProvider.hashPrefix('!');
		
		//url router provider
		$urlRouterProvider.otherwise('/')

		//states
		$stateProvider
			.state('public', {
				abstract: true,
				url: '/',
				views: {
					"": {
						templateUrl: 'client/html/_layout.ng.html',
					},
					"header@public": {
						templateUrl: 'client/html/partials/header.ng.html',
						controller: 'headerController',
						controllerAs: 'header'
					}
				},
				onEnter: ['auth', '$state', '$meteor', '$log', 'ui', function(auth,
					$state: angular.ui.IStateService, $meteor: IHFMeteorService,
					$log: angular.ILogService,
					ui: UiService) {
					$log.info("public root");
					if (auth.isAuthenticated) {
						$log.debug('Authorized! as', auth.profile.email, auth.profile.id);
						ui.ShowSideMenu();
					} else {
						ui.HideSideMenu();
					}
				}]
			}).state('public.anonimus', {
				// todo : rename anonimus -> anonymous
				abstract: true,
				url: "^/anonimus",
				onEnter: ['$log', '$meteor', '$state', 'ui', 'auth',
					function(
						$log: angular.ILogService,
						$meteor: IHFMeteorService,
						$state: ng.ui.IStateService,
						ui: UiService,
						auth) {
						$log.info('public.anonimus');
						ui.HideSideMenu();
						if (auth.isAuthenticated) {
							ui.ShowSideMenu();
							// $log.debug('user is not anonimus. Redirecting to feed', auth.profile.id);
							auth.profilePromise.then(() => {
								$state.go('public.profile.feed', { userId: auth.profile.id });
							}).catch((err) => {
								$log.error(err);
							})
						}
						// $meteor.waitForUser().then((user) => {
						// 	if (user != null) {
						// 		$state.go('public.profile.feed', { userId: user._id });
						// 	}
						// });
					}],
				template: '<ui-view></ui-view>'
			}).state("public.anonimus.registration", {
				url: '^/',
				onEnter: ['$log', function($log: angular.ILogService) {
					$log.info('registration');
				}],
				templateUrl: 'client/html/views/registration.ng.html',
				controller: 'signupController',
				controllerAs: 'signup'
			})
			.state('public.anonimus.registration.login', {
				url: '^/login',
				onEnter: ['temp', function(temp: TempService) {
					temp.openLogin();
				}]
			})
			.state('public.anonimus.restore', {
				url: '^/password/restore',
				onEnter: [function() { }]
			})
			.state('public.profile', {
				url: '^/profile/:userId',
				abstract: true,
				templateUrl: 'client/html/_profile.public.ng.html',
				controller: 'profilePublicController',
				controllerAs: 'profilePublic'
			})
			.state('public.profile.feed', {
				url: '/feed',
				templateUrl: 'client/html/views/profile.feed.ng.html',
				controller: function($scope) {
					$scope.posts = [1, 2, 3]
				}
			})
			.state('public.profile.info', {
				url: '/info',
				templateUrl: 'client/html/views/profile.info.ng.html',
				controller: 'profileInfoController',
				controllerAs: 'profileInfo'
			})
			.state('public.profile.interests', {
				url: '/interests',
				templateUrl: 'client/html/views/profile.interests.ng.html',
				controller: function() { }
			})
			.state('public.profile.groups', {
				url: '/groups',
				templateUrl: 'client/html/views/profile.groups.ng.html',
				controller: function() { }
			})
			.state('public.profile.subsbar', {
				abstract: true,
				templateUrl: 'client/html/views/subscriptions.bar.ng.html',
				controller: 'subsBarController',
				controllerAs: 'subsBar'
			})
			.state('public.profile.subsbar.subscriptions', {
				url: '/subscriptions',
				templateUrl: 'client/html/views/profile.subscriptions.ng.html',
				controller: 'subscriptionsController',
				controllerAs: 'subsc'
			})
			.state('public.profile.subsbar.followers', {
				url: '/followers',
				templateUrl: 'client/html/views/profile.followers.ng.html',
				controller: 'followersController',
				controllerAs: 'fc'
			})
			.state('public.we', {
				url: '^/we',
				templateUrl: 'client/html/views/public.we.ng.html',
				controller: 'weController',
				controllerAs: 'we'
			})
			.state('public.uikit', {
				url: '^/ui-kit',
				templateUrl: "client/html/ui/kit.ng.html",
				controller: ['$scope', 'temp', function($scope, temp: TempService) {
					$scope.openCompleteSignUp = function(user: IHFUser) {
						temp.openCompleteSignUp(user);
					}
					$scope.openLogin = function() {
						temp.openLogin();
					};
					$scope.openNewPassword = function() {
						temp.openNewPassword();
					};
					$scope.openReset = function() {
						temp.openReset();
					};
					$scope.openRestore = function() {
						temp.openRestore();
					};
					$scope.subName = pubsubNames.users.userById;
					$scope.coll = Meteor.users;
				}]
			})
			.state('private', {
				abstract: true,
				url: '/',
				// controller: function() { },
				onEnter: ['auth', '$state', '$meteor', '$log', 'ui', function(auth,
					$state: angular.ui.IStateService, $meteor: IHFMeteorService,
					$log: angular.ILogService,
					ui: UiService) {
					$log.info("private State");
					//МБ стоит использовать $rootScope.currentUser
					// $meteor.waitForUser().then((user) => {
					$log.debug('private root');
					if (!auth.isAuthenticated) {
						// $log.debug('non authorized! auth', auth.profile);
						$state.go('public.anonimus.registration.login');
					} else {
						$log.debug('authorized', auth.profile);
						ui.ShowSideMenu();
					}
					// });
				}],
				views: {
					"": {
						templateUrl: 'client/html/_layout.ng.html',
					},
					"header@private": {
						templateUrl: 'client/html/partials/header.ng.html',
						controller: 'headerController',
						controllerAs: 'header'
					}
				}
			})
			.state('private.profile', {
				url: '^/profile',
				abstract: true,
				templateUrl: 'client/html/_profile.private.ng.html',
				controller: 'profilePrivateController',
				controllerAs: 'profilePrivate',
				resolve: {
					user: ['$meteor', function($meteor: IHFMeteorService) {
						return $meteor.waitForUser();
					}]
				}
			})
			.state('private.profile.settings', {
				url: '/settings',
				templateUrl: 'client/html/views/profile.settings.ng.html',
				controller: 'profileSettingsController',
				controllerAs: 'profileSettings'
			})
			.state('private.profile.themes', {
				url: '/themes',
				templateUrl: 'client/html/views/themes.ng.html',
				controller: 'themeController',
				controllerAs: 'themeController'
			})
			.state('private.im', {
				url: '^/im',
				resolve: {
					user: ['$meteor', ($meteor: IHFMeteorService) => {
						return $meteor.waitForUser();
					}]
				},
				templateUrl: 'client/html/views/im.ng.html',
				controller: 'imController',
				controllerAs: 'im'
			})
			.state('private.im.chat', {
				url: '/:id',
				templateUrl: 'client/html/views/im.messages.ng.html',
				controller: 'imChatController',
				controllerAs: 'chat'
			})
			.state('private.feed', {
				url : '^/feed',
				templateUrl : 'client/html/views/feed.ng.html',
				controller : 'privateFeedController',
				controllerAs : 'feed',
				
				
			})

	}]);

hfclient.run(['$rootScope', '$state', '$modalStack', 'ui', 'auth',
	function($rootScope: IHFRootScope, $state: ng.ui.IStateService,
		$modalStack: ng.ui.bootstrap.IModalStackService,
		ui: UiService,
		auth,
		$meteor: IHFMeteorService) {
		$rootScope.auth = auth;
		$rootScope['$meteor'] = $meteor;
		$rootScope['$state'] = $state;
		$rootScope.ui = ui;
		$rootScope.$on('$stateChangeStart',
			function(event, toState, toParams, fromState, fromParams) {
				// $modalStack.dismissAll("locationChanged");
			})
	}]);