/// <reference path='../lib/app.ts' />

class SubscriptionsService {
	static $inject = ['$log', '$q', '$meteor'];
	constructor(
		private $log: ng.ILogService,
		private $q: angular.IQService,
		private $meteor: IHFMeteorService) {

	}

	IsSubscribedOn(subscrUserId: string): ng.IPromise<boolean> {
		var defer = this.$q.defer()
		this.$meteor.call<boolean>(methodNames.subscriptions.imSubscribed, subscrUserId)
			.then((subscribed) => {
				defer.resolve(subscribed);
			}).catch((err) => {
				defer.reject(err);
			});
		return defer.promise;
	}

	Subscribe(subscribeTo: string): ng.IPromise<string> {
		var defer = this.$q.defer()
		this.$meteor.call(methodNames.subscriptions.subscribe, subscribeTo)
			.then((subscriptionId) => {
				defer.resolve(subscriptionId);
			}).catch((err) => {
				defer.reject(err);
			})
		return defer.promise;
	}
	UnSubscribe(unsubscribeFrom: string): ng.IPromise<boolean> {
		var defer = this.$q.defer()
		this.$meteor.call(methodNames.subscriptions.unsubscribe, unsubscribeFrom)
			.then((result: boolean) => {
				defer.resolve(result);
			}).catch((err) => {
				defer.reject(err);
			})
		return defer.promise;
	}
}
angular.module('hf-client').service('subscriptionsService', SubscriptionsService);