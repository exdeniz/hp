/// <reference path='../lib/app.ts' />

class UiService {
	static $inject = ['$log']
	constructor(private $log: ng.ILogService) {
		this.isShowSideMenu = true;
	}
	private isShowSideMenu = true
	ShowSideMenu() {
		this.$log.debug('ShowingSideMenu');
		this.isShowSideMenu = true;
	}
	HideSideMenu() {
		this.$log.debug('HidingSideMenu');
		this.isShowSideMenu = false;
	}
}
angular.module('hf-client').service('ui', UiService);