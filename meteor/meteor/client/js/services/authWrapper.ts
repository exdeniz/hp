/// <reference path='../lib/app.ts' />
class AuthWrapper {
    static $inject = ['auth', '$q', '$log'];
    constructor(
        public auth,
        public $q: angular.IQService,
        public $log: ng.ILogService
    ) { }
    login(email, password) {
        this.$log.debug('logging in');
        var dfd = this.$q.defer<any>();
        this.auth.passwordGrant(email, password).then((userdata) => {
            this.$log.debug('logginned in');
            dfd.resolve(userdata);
        }, (err) => {
            this.$log.error('loggin error', err);
            dfd.reject();
        });
        return dfd.promise;
    };
    signUp(model: {
        email: string,
        password: string
    }): ng.IPromise<any> {
        this.$log.debug('signing up');
        return this.auth.user.signup(model).then(() => {
            this.$log.debug('signed up');
            return this.login(model.email, model.password);
        });
    }

}

angular.module('hf-client').service('authWrapper', AuthWrapper);