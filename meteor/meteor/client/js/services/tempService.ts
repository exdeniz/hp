/// <reference path='./authWrapper.ts' />
/// <reference path='./uiService.ts' />
interface ILoginModel {
    error: boolean;
    email?: string;
    password?: string
}

interface ILoginModalScope extends IHFModalScope {
    model: ILoginModel;
    login: (form: angular.IFormController) => ng.IPromise<void>;
    openRestore: () => void;
    loginSocial: (SocialProviders) => void
}

class TempService {
    static $inject = ['$modal', 'auth', '$modalStack'];
    constructor(
        private $modal: angular.ui.bootstrap.IModalService,
        private auth,
        private $modalStack: angular.ui.bootstrap.IModalStackService,
        private ui: UiService
    ) { }
    /**
     * Модальное окно логина пользователя
     */
    openLogin() {
        this.$modalStack.dismissAll();
        this.$modal.open({
            size: 'wide',
            animation: true,
            backdrop: 'static',
            templateUrl: 'client/html/modals/login.ng.html',
            controller: ['auth',
                '$scope',
                '$modalInstance',
                '$q',
                'authWrapper',
                '$meteor',
                '$log',
                '$state',
                '$timeout',
                'ui',
                (auth,
                    $scope: ILoginModalScope,
                    $modalInstance: angular.ui.bootstrap.IModalServiceInstance, $q: angular.IQService, authWrapper: AuthWrapper,
                    $meteor: IHFMeteorService,
                    $log: ng.ILogService,
                    $state: ng.ui.IStateService,
                    $timeout: ng.ITimeoutService,
                    ui: UiService) => {
                    $scope.openRestore = () => this.openRestore();
                    $scope.model = {
                        error: false,
                    };

                    $scope.loginSocial = (provider: SocialProviders) => {
                        $log.debug('social provider', provider);
                        auth.oauth.popup(provider)
                            .then((si) => {
                                ///Ищем пользователя у нас в бд.
                                return this.auth.socialLogin(si).then(function() {
                                    //Пользователь найден и залогинен
                                    //state.go можно использовать
                                    location.reload();
                                }).catch(() => {
                                    ///пользователь новый, Получаем информацию о пользователе
                                    return this.auth.oauth.me(si)
                                        .then((me) => {
                                            //Регистрируем
                                            return this.auth.user.signup(me)
                                                .then(() => {
                                                    //Входим
                                                    this.auth.socialLogin(me)
                                                        .then(() => {
                                                            //выставить аватарку, задеплоить данные о пользователе.///$meteor.call('updateUserData',userdata);
                                                            //потом открыть попап
                                                            $meteor.waitForUser().then((user) => {
                                                                this.openCompleteSignUp(user).catch(() => {
                                                                    $state.go('public.profile.info', { userId: user._id });
                                                                });
                                                            });
                                                        })
                                                        .catch(() => {
                                                            $log.error('ошибка входа');
                                                        });
                                                }, () => {
                                                    $log.error("Email привязанный к этой записи уже используется")
                                                });
                                        })
                                })
                            }, () => {
                                $log.error("Не удалось открыть всплывающее окно");
                            });
                    }

                    $scope.login = function(form: ng.IFormController) {
                        var dfd = $q.defer<void>();
                        if (form.$invalid) {
                            dfd.reject();
                            return dfd.promise;
                        }
                        $scope.model.error = false;
                        return authWrapper.login($scope.model.email, $scope.model.password)
                            .then(function(userdata) {
                                $timeout(() => {
                                    $modalInstance.close();
                                    $meteor.waitForUser().then((user) => {
                                        $log.debug(user);
                                        $state.go('public.profile.feed', { userId: user._id });
                                        ui.ShowSideMenu();
                                    });
                                });
                            }, function() {
                                $scope.model.error = true;
                            });
                    }
                }]
        })
    }
    /**
     * Модальное окно восстановления пароля по email
     */
    openRestore() {
        this.$modalStack.dismissAll();
        this.$modal.open({
            templateUrl: 'client/html/modals/restore.ng.html',
            controller: ['auth', '$scope', '$modalInstance', (auth, $scope, $modalInstance: angular.ui.bootstrap.IModalServiceInstance) => {
                $scope.model = {};
                $scope.restore = (form: angular.IFormController) => {
                    if (form.$invalid) {
                        return;
                    }
                    return auth.user.restore($scope.model.email)
                        .then((result2) => {
                            $modalInstance.close();
                            this.openRestoreConfirm($scope.model.email);
                        });
                }
            }]
        })
    }
    /**
     * Модальное окно - письмо восстановления пароля отправлено
     */
    openRestoreConfirm(email: string) {
        this.$modalStack.dismissAll();
        this.$modal.open({
            templateUrl: 'client/html/modals/reset.ng.html',
            controller: ['$scope', '$modalInstance', function($scope) {
                $scope.model = {};
                var domain = email.split('@')[1];
                if (domain) {
                    $scope.model.domain = domain;
                }
            }]
        });
    }
    /**
     * Модальное окно, открывающееся при переходе по ссылке восстаговления пароля
     */
    openReset() {
        this.$modalStack.dismissAll();
        this.$modal.open({
            templateUrl: 'client/html/modals/reset.ng.html',
            controller: ['auth', '$scope', '$modalInstance', '$state', (auth, $scope,
                $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
                $state: angular.ui.IStateService) => {
                $scope.model = {
                    login: $state.params['login'],
                    token: $state.params['token']
                };

                $scope.reset = function(form) {
                    if (form.$invalid) {
                        return;
                    }

                    return auth.user.reset($scope.model)
                        .then(function(result2) {
                            $modalInstance.close();
                        });
                }
            }]
        })
    }
    /**
     * Модальное окно подтверждения регистрации
     */
    openCompleteSignUp(user: IHFUser) {
        this.$modalStack.dismissAll();
        return this.$modal.open({
            templateUrl: 'client/html/modals/complete.ng.html',
            // resolve: {
            //     user: ['$meteor', function($meteor: ng.meteor.IMeteorService) {
            //         return <ng.IPromise<IHFUser>>$meteor.waitForUser();
            //     }]
            // },
            controller: ['$scope', '$state', function($scope: any, $state: ng.ui.IStateService) {
                $scope.$state = $state;
            }]
        }).result.finally(() => {
            this.ui.ShowSideMenu();
        });
    }
    /**
     * Модальное окно смены пароля
     */
    openNewPassword() {
        this.$modalStack.dismissAll();
        this.$modal.open({
            templateUrl: 'client/html/modals/newpassword.ng.html',
            controller: ['auth', '$scope', '$modalInstance', (auth, $scope, $modalInstance: angular.ui.bootstrap.IModalServiceInstance) => {
                $scope.changePassword = function(form: angular.IFormController) {
                    if (form.$invalid) {
                        return;
                    }
                    return auth.user.changePassword($scope.model.password)
                        .then(function(result2) {
                            $modalInstance.close();
                        });
                }
            }]
        })
    }
    
    /**
     * Модальное окно crop
     */
    openCrop(imgData: string, type: CropType) {
        this.$modalStack.dismissAll();
        var tUrl: string = 'client/html/modals/cropAvatar.ng.html';
        switch (type) {
            case CropType.avatar: tUrl = tUrl; break;
            case CropType.cover: tUrl = 'client/html/modals/cropCover.ng.html'; break;
        }
        return this.$modal.open({
            templateUrl: tUrl,
            controller: [
                '$scope',
                '$modalInstance',
                '$q',
                ($scope: any,
                    $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
                    $q: ng.IQService) => {
                    $scope.model = {
                        myImage: imgData,
                        myCroppedImage: ''
                    };

                    $scope.crop = function(data: string) {
                        $modalInstance.close(data);
                    };
                }]
        });
    }
}
angular.module('hf-client').service('temp', TempService);

// hfclient
// 	.service('temp', ['$modal', '$q', 'auth', '$modalStack', 'authWrapper',
// 		function(
// 			$modal: angular.ui.bootstrap.IModalService,
// 			$q: angular.IQService,
// 			auth,
// 			$modalStack: angular.ui.bootstrap.IModalStackService,
// 			authWrapper: AuthWrapper) {
// 			var that = this;
// 			that.openLogin = function() {
// 				$modalStack.dismissAll();
// 				$modal.open({
// 					templateUrl: 'client/html/modals/login.ng.html',
// 					controller: ['auth', '$scope', '$modalInstance', function(auth, $scope: ILoginModalScope,
// 						$modalInstance: angular.ui.bootstrap.IModalServiceInstance) {
// 						$scope.openRestore = that.openRestore;
// 						$scope.model = {
// 							error: false,
// 						};
// 						$scope.login = function(form) {
// 							var dfd = $q.defer<void>();
// 							if (form.$invalid) {
// 								dfd.reject();
// 								return dfd.promise;
// 							}
// 							$scope.model.error = false;

// 							return authWrapper.login($scope.model.email, $scope.model.password)
// 								.then(function() {
// 									$modalInstance.close();
// 								}, function() {
// 									$scope.model.error = true;
// 								});
// 						}
// 					}]
// 				})
// 			}

// 			// that.openSignup = function() {
// 			// 	$modalStack.dismissAll();
// 			// 	$modal.open({
// 			// 		templateUrl: 'views/modal/signup.html',
// 			// 		controller: ['auth', '$scope', '$modalInstance', function(auth, $scope, $modalInstance: angular.ui.bootstrap.IModalServiceInstance) {
// 			// 			$scope.model = {
// 			// 				error: false
// 			// 			};
// 			// 			$scope.signup = function(form: angular.IFormController) {
// 			// 				if (form.$invalid) {
// 			// 					return;
// 			// 				}

// 			// 				$scope.$root.auth.user.signup({
// 			// 					email: $scope.model.email,
// 			// 					password: $scope.model.password
// 			// 				}).then(function(result) {
// 			// 					return authWrapper.login($scope.model.email, $scope.model.password)
// 			// 						.then(function(result2) {
// 			// 							// form.error = false;
// 			// 							$modalInstance.close();
// 			// 						});
// 			// 				}, function(error) {
// 			// 					// form.error = true;
// 			// 				});

// 			// 			}
// 			// 		}]
// 			// 	})
// 			// };

// 			that.openRestore = function() {
// 				$modalStack.dismissAll();
// 				$modal.open({
// 					templateUrl: 'client/html/modals/restore.ng.html',
// 					controller: ['auth', '$scope', '$modalInstance', function(auth, $scope, $modalInstance: angular.ui.bootstrap.IModalServiceInstance) {
// 						$scope.model = {};
// 						$scope.restore = function(form: angular.IFormController) {
// 							if (form.$invalid) {
// 								return;
// 							}
// 							return auth.user.restore($scope.model.email)
// 								.then(function(result2) {
// 									$modalInstance.close();
// 								});
// 						}
// 					}]
// 				})
// 			}

// 			that.openReset = function() {
// 				$modalStack.dismissAll();
// 				$modal.open({
// 					templateUrl: 'client/html/modals/reset.ng.html',
// 					controller: ['auth', '$scope', '$modalInstance', '$state', function(auth, $scope,
// 						$modalInstance: angular.ui.bootstrap.IModalServiceInstance,
// 						$state: angular.ui.IStateService) {
// 						$scope.model = {
// 							login: $state.params.login,
// 							token: $state.params.token
// 						};

// 						$scope.reset = function(form) {
// 							if (form.$invalid) {
// 								return;
// 							}

// 							return auth.user.reset($scope.model)
// 								.then(function(result2) {
// 									$modalInstance.close();
// 								});
// 						}
// 					}]
// 				})
// 			}

// 			// that.openBid = function(auctionId, price, buyNow, maxBid, currentUserBestBid, bids) {
// 			// 	$modalStack.dismissAll();
// 			// 	var tcs = $q.defer();
// 			// 	if (auth.isAuthenticated) {
// 			// 		$rootScope.loadProfile().then(function() {
// 			// 			tcs.resolve();
// 			// 		})
// 			// 	} else {
// 			// 		$rootScope.authCallback = function() {
// 			// 			$rootScope.loadProfile().then(function() {
// 			// 				tcs.resolve();
// 			// 			})
// 			// 		}
// 			// 		that.openLogin();
// 			// 	}

// 			// 	tcs.promise.then(function() {
// 			// 		$modal.open({
// 			// 			templateUrl: 'views/modal/bid.html',
// 			// 			windowClass: 'bid',
// 			// 			keyboard: false,
// 			// 			backdrop: 'static',
// 			// 			controller: ['$scope', 'experimentService', '$http', 'auth', function($scope, experimentService, $http, auth) {
// 			// 				$scope.experimentId = experimentService.experimentId;
// 			// 				// $scope.buyNowPrice = buyNow;
// 			// 				$scope.maxBid = maxBid;
// 			// 				$scope.model = {
// 			// 					isSelfDelivery: true,
// 			// 					nowIsSelfDelivery: true,
// 			// 					price: price,
// 			// 					check: true
// 			// 				};

// 			// 				$scope.currentUserBestBid = currentUserBestBid;
// 			// 				$scope.auth = auth;
// 			// 				$scope.bids = bids;
// 			// 				$scope.isBidNotificationCollapsed = false;

// 			// 				// $scope.toogleDeliveryAddress = function () {
// 			// 				// 	$scope.model.isShowCustomAddress = !$scope.model.isShowCustomAddress;
// 			// 				// 	$scope.model.delivery = {};
// 			// 				// };
						
// 			// 				// $scope.searchDeliveryAddresses = function () {
// 			// 				// 	$http.get('/api/address/searchDeliveryAddresses')
// 			// 				// 		.success(function (data) {
// 			// 				// 			$scope.asyncDeliveryAddresses = data.items;
// 			// 				// 			$scope.deliveryAddressMap = data.map;
// 			// 				// 		})
// 			// 				// };

// 			// 				// $scope.searchDeliveryAddresses();

// 			// 				// $scope.buyNow = function () {
// 			// 				// 	$scope.loading = $http.post('/api/vehicle/buyNow', {}, { params: { auctionId: auctionId, buyNowPrice: $scope.buyNowPrice } })
// 			// 				// 		.success(function () {
// 			// 				// 			that.openThanks();
// 			// 				// 		})
// 			// 				// 		.error(function (error, code) {
// 			// 				// 			toastr.error('Невозможно сделать ставку. Пожалуйста, попробуйте позже.');
// 			// 				// 		});
// 			// 				// }
// 			// 				$scope.error = undefined;
// 			// 				$scope.bid = function(form) {
// 			// 					if (form.$invalid) return;

// 			// 					$scope.loading = $http.post('/api/vehicle/createBid', $scope.model, { params: { auctionId: auctionId } })
// 			// 						.success(function() {
// 			// 							that.openThanks();
// 			// 							$rootScope.loadProfile();
// 			// 							$rootScope.reloadPage();
// 			// 						})
// 			// 						.error(function(error, code) {
// 			// 							if (code == 400) {
// 			// 								$scope.error = error.message;
// 			// 							} else {
// 			// 								toastr.error(error.message || 'Невозможно сделать ставку. Пожалуйста, попробуйте позже.');
// 			// 							}
// 			// 						});
// 			// 				}
// 			// 			}]
// 			// 		})
// 			// 	});
// 			// }

// 			// that.openThanks = function() {
// 			// 	$modalStack.dismissAll();
// 			// 	$modal.open({
// 			// 		templateUrl: 'views/modal/thanks.html'
// 			// 	})
// 			// }
// 		}])
