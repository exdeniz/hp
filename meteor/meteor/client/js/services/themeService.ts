/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/Theme.d.ts' />
/// <reference path='../../../lib/ThemesCollection.ts' />
/// <reference path='../../../lib/PublicationNames.ts' />
/// <reference path="../../../typings/tsd.d.ts" />

interface IThemesData {
	rootThemes: Theme[];
	subscriptions: ThemeSubscription[];
	subscriptionIds: string[];
	themes: _.Dictionary<Theme>;
	searchChildrenResult: Theme[];
}

class ThemeService {
	static $inject = ['$q', '$meteor', '$modal', '$modalStack'];
	public data: IThemesData = <IThemesData>{};

	constructor(
		private $q: ng.IQService,
		private $meteor: IHFMeteorService,
		private $modal: angular.ui.bootstrap.IModalService,
		private $modalStack: angular.ui.bootstrap.IModalStackService) {
		this.init();
	}

	init(): ng.IPromise<UserThemesResult> {
		var def = this.$q.defer();
		this.getThemes().then((themes) => {
			this.data.rootThemes = this.getRootThemes(themes);
			this.getThemesSubscriptions().then((subscriptions) => {
				var result = this.getUserThemes(themes, subscriptions);
				def.resolve(result);
				this.data.themes = result.themes;
				this.data.subscriptions = result.subscriptions;

			});
		});
		return def.promise;
	}

	getThemes(): ng.IPromise<Theme[]> {
		var def = this.$q.defer();
		this.$meteor.subscribe(pubsubNames.themes.all).then(
			() => {
				var themes = this.$meteor.collection<Theme>(Themes, false);
				def.resolve(themes);
			},
			(error) => {
				def.reject(error);
			});
		return def.promise;
	}

	getThemesSubscriptions(): ng.IPromise<ThemeSubscription[]> {
		var def = this.$q.defer();
		this.$meteor.subscribe(pubsubNames.themes.subscriptions).then(
			() => {
				var subscriptions = this.$meteor.collection<ThemeSubscription>(ThemesSubscriptions, false);
				def.resolve(subscriptions);
			},
			(error) => {
				def.reject(error);
			});
		return def.promise;
	}

	getRootThemes(themes: Theme[]) {
		return _.filter(themes, t => !t.rootId);
	}

	getUserThemes(themes: Theme[], subscriptions: ThemeSubscription[]): UserThemesResult {
		var themeMap = _.indexBy<Theme>(themes, s => s._id);
		return <UserThemesResult>{
			subscriptions: subscriptions,
			themes: themeMap
		};
	}

	openModal(select: boolean = false, rootTheme?: Theme, parentTheme?: Theme): ng.IPromise<Theme> {
		this.$modalStack.dismissAll();
        var modal = this.$modal.open({
            templateUrl: 'client/html/modals/theme.ng.html',
            controller: ['$scope', '$modalInstance', ($scope, $modalInstance: angular.ui.bootstrap.IModalServiceInstance) => {
				// if(!rootTheme){
				// 	rootTheme = this.data.rootThemes[0];
				// }
				$scope.parentId = parentTheme ? parentTheme._id : rootTheme ? rootTheme._id : undefined;
				$scope.term = '';
				$scope.rootTheme = rootTheme;
				$scope.breadCrumbs = [];
				$scope.showAddButton = false;

				this.data.subscriptionIds = _.pluck(this.data.subscriptions, 'themeId');

				this.$meteor.autorun($scope, () => {
					var parentId = $scope.getReactively('parentId');
					if (!parentId) {
						$scope.themes = _(this.data.rootThemes).map((t) => {
							t['__originalId'] = t._id;
							return t;
						});
						//return;
					}
					else {
						$scope.themes = HFThemesIndex.search($scope.getReactively('term'), {
							limit: 10,
							props: {
								parentId: $scope.getReactively('parentId')
							}
						}).fetch();
					}

					var theme = _.find($scope.themes, (t: Theme) => { return t.name == $scope.term });
					if ($scope.term && !theme) {
						$scope.showAddButton = true;
					}
					else {
						$scope.showAddButton = false;
					}
					
					// Check selected or not
					if (!select) {
						_.map($scope.themes, (theme) => {
							if (_.contains(this.data.subscriptionIds, theme['__originalId'])) {
								theme['selected'] = true;
							}
							else {
								theme['selected'] = false;
							}
						})
					}
				});

				$scope.checkTheme = (theme) => {
					if (select) {
						theme['selected'] = false;
						this.$modalStack.close(modal, theme);
						return;
					}
					theme['selected'] ? this.subscribe(theme.__originalId) : this.unsubscribe(theme.__originalId);
				}

				$scope.createTheme = () => {
					if(!$scope.term) return;
					var theme: Theme = {
						name: $scope.term,
						parentId: $scope.parentId,
						rootId: $scope.rootTheme._id
					};
					this.$meteor.call(methodNames.themes.create, theme).then(
						(theme: Theme) => {
							this.data.themes[theme._id] = theme;
						},
						(error) => { console.log(error) }
					);
				}

				var setBreadCrumbs = (parentId) => {
					var parentTheme: Theme = this.data.themes[parentId];
					if (parentTheme && parentTheme.parentId != parentId) {
						$scope.breadCrumbs.unshift(parentTheme);
						setBreadCrumbs(parentTheme.parentId);
					}
				};

				setBreadCrumbs($scope.parentId);

				$scope.setParent = (parentId) => {
					$scope.parentId = parentId;
					$scope.breadCrumbs = [];
					setBreadCrumbs(parentId);
				}
            }],
			resolve: function(theme) {
				return theme;
			}
        });
		return modal.result;
	}

	subscribe(themeId: string) {
		return this.$meteor.call(methodNames.themes.subscribe, themeId).then(
			() => {
				this.data.subscriptionIds.push(themeId);
			},
			(error) => { console.log(error) }
		);
	}

	unsubscribe(themeId: string) {
		this.$meteor.call(methodNames.themes.unsubscribe, themeId).then(
			() => {
				this.data.subscriptionIds = _.without(this.data.subscriptionIds, themeId);;
			},
			(error) => { console.log(error) }
		);
	}
}

angular.module('hf-client').service('themeService', ThemeService);