/// <reference path='../../../typings/tsd.d.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../services/uiService.ts' />
interface IHFRootScope extends angular.IRootScopeService, angular.meteor.IRootScopeService {
	currentUser: IHFUser;
	ui: UiService;
	auth: any;
}

interface IHFMeteorService extends angular.meteor.IMeteorService {
	/**
         * Returns a promise fulfilled with the currentUser when the user subscription is ready. 
         * This is useful when you want to grab the current user before the route is rendered.
         * If there is no logged in user, it will return null. 
         * See the “Authentication with Routers” section of our tutorial for more information and a full example.
         */
	waitForUser(): angular.IPromise<IHFUser>;
	/**
		* Resolves the promise successfully if a user is authenticated and rejects otherwise. 
		* This is useful in cases where you want to require a route to have an authenticated user. 
		* You can catch the rejected promise and redirect the unauthenticated user to a different page, such as the login page. 
		* See the “Authentication with Routers” section of our tutorial for more information and a full example.
		*/
	requireUser(): angular.IPromise<IHFUser>;
	/**
		* Resolves the promise successfully if a user is authenticated and the validatorFn returns true; rejects otherwise. 
		* This is useful in cases where you want to require a route to have an authenticated user and do extra validation like the user's role or group. 
		* You can catch the rejected promise and redirect the unauthenticated user to a different page, such as the login page. 
		* See the “Authentication with Routers” section of our tutorial for more information and a full example.
		* 
		* The mandatory validator function will be called with the authenticated user as the single param and it's expected to return true in order to resolve. 
		* If it returns a string, the promise will be rejected using said string as the reason. 
		* Any other return (false, null, undefined) will be rejected with the default "FORBIDDEN" reason.
		*/
	requireValidUser(validatorFn: (user: IHFUser) => boolean | string): angular.IPromise<IHFUser>;
}

interface IHFScope extends angular.IScope, angular.meteor.IScope {
	$root: IHFRootScope;
	currentUser: IHFUser;
	$meteorAutorun(autorun: () => void);
	$meteorCollectionFS<T>(collection: Mongo.Collection<T>, autoClientSave?: boolean): angular.meteor.AngularMeteorCollection<T>;
	$meteorCollectionFS<T, U>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave: boolean, updateCollection: Mongo.Collection<U>): angular.meteor.AngularMeteorCollection2<T, U>;
	/**
	* A service that wraps the Meteor collections to enable reactivity within AngularJS.
	* 
	* @param collection - A Meteor Collection or a reactive function to bind to. 
	*                  - Reactive function can be used with $scope.getReactively to add $scope variable as reactive variable to the cursor.
	* @param [autoClientSave=true] - By default, changes in the Angular collection will automatically update the Meteor collection. 
	*                              - However if set to false, changes in the client won't be automatically propagated back to the Meteor collection.
	*/
	$meteorCollection<T>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave?: boolean): angular.meteor.AngularMeteorCollection<T>;
        
	/**
	 * A service that wraps the Meteor collections to enable reactivity within AngularJS.
	 * 
	 * @param collection - A Meteor Collection or a reactive function to bind to. 
	 *                   - Reactive function can be used with $scope.getReactively to add $scope variable as reactive variable to the cursor.
	 * @param [autoClientSave=true] - By default, changes in the Angular collection will automatically update the Meteor collection. 
	 *                              - However if set to false, changes in the client won't be automatically propagated back to the Meteor collection.
	 * @param [updateCollection] - A collection object which will be used for updates (insert, update, delete).
	 */
	$meteorCollection<T, U>(collection: Mongo.Collection<T> | angular.meteor.ReactiveResult | Function | (() => T), autoClientSave: boolean, updateCollection: Mongo.Collection<U>): angular.meteor.AngularMeteorCollection2<T, U>;
	
	/**
	* A service that wraps a Meteor object to enable reactivity within AngularJS. 
	* Finds the first document that matches the selector, as ordered by sort and skip options. Wraps collection.findOne
	* 
	* @param collection - A Meteor Collection to bind to.
	* @param selector - A query describing the documents to find or just the ID of the document. 
	*                 - $meteor.object will find the first document that matches the selector, 
	*                 - as ordered by sort and skip options, exactly like Meteor's collection.findOne
	* @param [autoClientSave=true] - By default, changes in the Angular object will automatically update the Meteor object. 
	*                              - However if set to false, changes in the client won't be automatically propagated back to the Meteor object.
	*/
	$meteorObject<T>(collection: Mongo.Collection<T>, selector: Mongo.Selector | Mongo.ObjectID | string, autoClientSave?: boolean): angular.meteor.AngularMeteorObject<T>;
	/**
  * A service which is a wrapper for Meteor.subscribe. It subscribes to a Meteor.publish method in the client and returns a AngularJS promise when ready.
  * 
  * @param name - Name of the subscription. Matches the name of the server's publish() call.
  * @param publisherArguments - Optional arguments passed to publisher function on server.
  * 
  * @return The promise solved successfully when subscription is ready. The success promise holds the subscription handle.
  */
	$meteorSubscribe(name: string, ...publisherArguments: any[]): angular.IPromise<Meteor.SubscriptionHandle>;

}
interface IHFModalScope extends angular.ui.bootstrap.IModalScope {
	$root: IHFRootScope;
	currentUser: IHFUser;
}