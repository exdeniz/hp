/// <reference path='../../../typings/tsd.d.ts'/>
/// <reference path='../interfaces/Scopes.d.ts'/>

angular.module('hf-client').directive('subscribeUnsubscribe', ['$log', 'auth', 'subscriptionsService', 
($log: ng.ILogService, auth: any, subscriptionsService: SubscriptionsService) => {
	return {
		scope: {
			subscribedOn: '=subscribedOn',
			subscribeClass: '@',
			unsubscribeClass: '@'
		},
		templateUrl: 'client/html/directives/subscribe.unsubscribe.ng.html',
		link: (scope: any, element: ng.IAugmentedJQuery, iAttrs: ng.IAttributes) => {
			scope.isAuthenticated = auth.isAuthenticated;
			scope.isSubscribed = false;
			subscriptionsService.IsSubscribedOn(scope.subscribedOn)
				.then((subscribed) => {
					scope.isSubscribed = subscribed;
				})
				.catch((err) => {
					$log.debug(err);
				});

			scope.subscribe = () => {
				subscriptionsService.Subscribe(scope.subscribedOn)
					.then((subscribeId: string) => {
						scope.isSubscribed = true;
					})
					.catch((err) => {
						$log.debug(err);
					});
			};

			scope.unsubscribe = () => {
				subscriptionsService.UnSubscribe(scope.subscribedOn)
					.then((resurl: boolean) => {
						scope.isSubscribed = false;
					})
					.catch((err) => {
						$log.debug(err);
					});
			};
		}
	}
}]);