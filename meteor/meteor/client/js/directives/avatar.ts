/// <reference path='../../../typings/tsd.d.ts'/>
/// <reference path='../interfaces/Scopes.d.ts'/>

angular.module('hf-client').directive('avatar', [function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {},
		template: '<img ng-src="{{avatar.url()}}" src="/img/avatar.jpg" />',
		link: function(scope: IHFScope, element: ng.IAugmentedJQuery, iAttrs: ng.IAttributes) {
			// var userId = iAttrs['userId'];
			iAttrs.$observe<string>('userId', (userId) => {
				if (userId) {
					scope.$meteorSubscribe(pubsubNames.images.avatars.current, userId).then(() => {
						scope['avatar'] = scope.$meteorObject(UserAvatars, { userId: userId }, false);
						if (!scope['avatar']._id) {
							scope['avatar']._id = "avatar" + userId;
						}
					});
				}
			})
		}
	};
}]);