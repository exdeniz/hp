/// <reference path='../interfaces/Scopes.d.ts' />
interface ISidebarScope extends IHFScope {
	logout: () => void;
}

function SidebarLeft(
	$meteor: ng.meteor.IMeteorService,
	$log: angular.ILogService,
	auth: any,
	$state: ng.ui.IStateService,
	ui: UiService) {
	return {
        link: function($scope: ISidebarScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes) {
			$scope.$meteorSubscribe(pubsubNames.subscriptions.followersNewCount)
				.then((countSubscHandle) => {
					// console.log('subscribed newSubscribersCount');
					$scope['newSubscribersCount'] = $scope.$meteorObject(Counts, pubsubNames.subscriptions.followersNewCount, false);
				});

			$scope.$meteorSubscribe(pubsubNames.chats.messagesNewCount).then(() => {
				$scope['newMessagesCount'] = $scope.$meteorObject(Counts, pubsubNames.chats.messagesNewCount, false);
			});

			$scope.logout = () => {
				$log.debug('logout');
				auth.signout();
				ui.HideSideMenu();
				if ($state.includes("private")) {
					$log.debug('anoni,usin private state... redirecting');
					$state.go('public.anonimus.registration');
				}
			}
        },
		templateUrl: 'client/html/directives/sidebar.left.ng.html'
    };
}
angular.module('hf-client').directive('sidebarLeft', ['$meteor', '$log', 'auth', '$state', 'ui', SidebarLeft]);
