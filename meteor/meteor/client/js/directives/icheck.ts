/// <reference path='../lib/app.ts' />

function ICheck($timeout: ng.ITimeoutService): ng.IDirective {
    //static $inject = ['$timeout'];
    return {
        require: 'ngModel',
        link: function($scope: ng.IScope, element: ng.IAugmentedJQuery, $attrs: ng.IAttributes, ngModel: ng.INgModelController) {
            return $timeout(function() {
                var value = $attrs['value'];
                var $element = <any>$(element);
                var round = $attrs['round'];

                // Instantiate the iCheck control.                            
                $element.iCheck({
                    checkboxClass: round ? 'iCheckRound' : 'iCheckBox',
                    radioClass: round ? 'iCheckRound' : 'iCheckBox',
                    //increaseArea: '20%'
                });

                // If the model changes, update the iCheck control.
                $scope.$watch($attrs['ngModel'], function(newValue) {
                    $element.iCheck('update');
                });

                // If the iCheck control changes, update the model.
                $element.on('ifChanged', function(event) {
                    if ($element.attr('type') === 'radio' && $attrs['ngModel']) {
                        $scope.$apply(function() {
                            ngModel.$setViewValue(value);
                        });
                    } else if ($element.attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(() => {
                            ngModel.$setViewValue($element[0].checked);
                        });
                    }
                });
            });
        }
    };
}

angular.module('hf-client').directive('iCheck', ['$timeout', ICheck]);