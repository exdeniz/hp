/// <reference path='../../../typings/tsd.d.ts'/>
/// <reference path='../interfaces/Scopes.d.ts'/>
/// <reference path='../../../shared/sharedEnums.ts' />
/// <reference path="../../../server/usersPublication.ts" />
/// <reference path="../../../lib/UserCollection.ts" />
/// <reference path="../../../interfaces/Event.d.ts" />


angular.module('hf-client').directive('postAdd', ['$log', 'auth', '$meteor',
	($log: ng.ILogService, auth: any, $meteor: ng.meteor.IMeteorService) => {
		return {
			replace: true,
			restrict: "E",
			templateUrl: 'client/html/directives/post.add.ng.html',
			link: (scope: any, element: ng.IAugmentedJQuery, iAttrs: ng.IAttributes) => {
				
				scope.test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
				iAttrs.$observe<string>('themeId', (themeId) => {
					scope.themeId = themeId;
				});
				scope.contexts = [
					{
						text: "Доступно всем",
						value: ContextTypes.Public
					}, {
						text: "Доступно мне",
						value: ContextTypes.Private
					}, {
						text: "Доступно семье",
						value: ContextTypes.Family
					}, {
						text: "Доступно хобби контексту",
						value: ContextTypes.Hobby
					}, {
						text: "Доступно бизнесс контексту",
						value: ContextTypes.Business
					},
				];
				scope.post = {
					title: "",
					text: "",
					filesId: [],
					themeId: "",
					files: [],
					linkedEventId : "",
					nestedEventId: "",
					dateRange: {
						startDateAt: new Date(),
						endDateAt: null
					},
					places: [],
					users: [],
					selectedContext: scope.contexts[0]
				};
				scope.tabs = [
					{//файлы
						name: 'attach',
						active: false,
						icon: '#iconAttach'
					}, {// даты события
						name: 'dates',
						active: false,
						icon: '#iconCalendar'
					}, { // местоположение события
						name: 'map',
						active: false,
						icon: '#iconMap'
					}, { // люди участвующие в событии
						name: 'people',
						active: false,
						icon: '#iconPeople'
					}
					// , { //связанные события
					// 	name: 'connect',
					// 	active: false,
					// 	icon: '#iconConnect'
					// }
				];
				scope.setActive = (tab) => {
					_(scope.tabs).each((t) => t.active = (tab === t));
				}
				scope.usersForAdd = [];
				scope.searchUsers = (term = "") => {
					var result = HFUsersIndex.search(term);
					$meteor.autorun(scope, () => {
						scope.usersForAdd = result.fetch();
					});
				}
				scope.publish = () => {
					// var event:IHFEvent = scope.post;
					$meteor.call(methodNames.events.addEvents, scope.post).then((result) => {
						$log.debug(result);
					}).catch((err) => {
						$log.error(err);
					});
				}
			}
		}
	}]);