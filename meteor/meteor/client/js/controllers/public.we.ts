/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
interface IWeScope extends IHFScope {
	founded: number;
	users: IHFUser[];
	term: string;
	perPage: number;
	page: number;
	sort: any;
	nextPage: (number) => void;
}
declare var userSearchResult: Mongo.Collection<IHFUser>
userSearchResult = new Mongo.Collection<IHFUser>('userSearchResult');
class WeController {
	static $inject = ['$stateParams', '$state', '$scope', '$log', '$meteor', 'auth'];
	constructor(
		private $stateParams: angular.ui.IStateParamsService,
		private $state: ng.ui.IStateService,
		private $scope: IWeScope,
		private $log: ng.ILogService,
		private $meteor: IHFMeteorService,
		private auth: any
	) {
		
		//users
		$scope.perPage = 10;
		$scope.page = 1;
		
		// var previusTerm;
		// $scope.$watch<string>('term', (n, o) => {
		// 	this.$log.debug('term:', n);
		// 	if (n) {
		// 		var sResult: Mongo.Cursor<IHFUser> = HFUsersIndex.search(n);
		// 		console.log('sResult', sResult);
		// 		console.log('sResult.fetch()', sResult.fetch());

		// 		$scope.users = sResult.fetch() // $scope.$meteorCollection<IHFUser>(sResult.)
		// 		this.$log.debug('$scope.users', $scope.users.length)
		// 	} else {
		// 		var sResult: Mongo.Cursor<IHFUser> = HFUsersIndex.search('');
		// 		console.log('sResult', sResult);
		// 		console.log('sResult.fetch()', sResult.fetch());

		// 		$scope.users = sResult.fetch() // $scope.$meteorCollection<IHFUser>(sResult.)
		// 		this.$log.debug('$scope.users', $scope.users.length)
		// 		// $scope.users = HFUsersIndex.search('');
		// 	}
		// });
		
		this.search('');

	}
	search(term) {
		this.$scope.users = [];
		this.$scope.founded = undefined;
		this.$meteor.autorun(this.$scope, () => {
			var searchResults = HFUsersIndex.search(term, {
				limit: this.$scope.getReactively('perPage'),
				skip: <number>this.$scope.getReactively('perPage') * (<number>this.$scope.getReactively('page') - 1)
			});
			this.$scope.founded = searchResults.count();
			this.$scope.users = _.uniq(this.$scope.users.concat(searchResults.fetch()), (u) => u['__originalId']);
		});
	}
}
angular.module('hf-client').controller('weController', WeController);