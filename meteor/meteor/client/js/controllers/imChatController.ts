/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../interfaces/Chat.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../interfaces/Scopes.d.ts' />
/// <reference path='../../../lib/ChatsCollection.ts' />
/// <reference path='../../../lib/PublicationNames.ts' />
interface IMCHatScope extends IImScope {
	chatId: string;
	chatSubsName: string;
	chatsCollection: any;
	userSubsName: string;
	usersCollection: any;
	newMessageText: string
}
class ImChatController {
	static $inject = ['$scope', '$meteor', '$stateParams', '$timeout'];
	constructor(
		private $scope: IMCHatScope,
		private $meteor: IHFMeteorService,
		private $stateParams: ng.ui.IStateParamsService,
		private $timeout: ng.ITimeoutService
	) {
		var messangerChatMessagesScrollOptions = {
			height: '100%',
			railVisible: true,
			railColor: '#fff',
			railOpacity: 1,
			color: '#dde1e5',
			size: '12px',
			alwaysVisible: true,
			borderRadius: '0px',
			railBorderRadius: '0px',
			//scrollTo: null
			//start: $('.messangerChatItem:last').length == 0 ? 'bottom' : $('.messangerChatItem:last') 
		};
		$scope.chatId = $stateParams['id'];
		$scope.newMessageText = '';
		$scope.chatSubsName = pubsubNames.chats.chatById;
		$scope.chatsCollection = Chats;
		$scope.userSubsName = pubsubNames.users.userById;
		$scope.usersCollection = Meteor.users;
		
		var getResetScrollFunc = (time) => _.debounce(function(){
			$timeout(function(){
				//messangerChatMessagesScrollOptions['start'] = $('.messangerChatItem:last');
				messangerChatMessagesScrollOptions['start'] = $('.messangerChatItem:last').length == 0 ? 'bottom' : $('.messangerChatItem:last');
			});
		}, time);
		var resetScroll = getResetScrollFunc(0);
		var initResetScroll = getResetScrollFunc(300);
		$scope.$meteorSubscribe(pubsubNames.chats.messages, $stateParams['id']).then(()=>{
			$scope.messages  =  $scope.$meteorCollection<IHFMessage>(() => {
			 	var cursor = Messages.find(
					{
						chatId: $stateParams['id']
					},
					{
						sort: {
							createdAt: 1
						}
					});
				cursor.observeChanges({
					added : function(id:string, fields : IHFMessage){
						//Если скролл в самом конце, скролим вниз
						var $scroll = $('#messagesScroll .slimScrollBar');
						if($scroll.height() + $scroll.position().top >= $scroll.parent().height()){
							initResetScroll();
						}
					}
				});
				initResetScroll();
				return cursor; }, false);
		});
		
		$scope.messangerChatMessagesScrollOptions = function(){return messangerChatMessagesScrollOptions};
	}
	sendMessage(text) {
		this.$meteor.call(methodNames.chats.sendMessage,{
			text,
			chatId : this.$stateParams['id']
		}).then(() => {
			this.$scope.newMessageText = '';
		});
	}
}

angular.module('hf-client').controller('imChatController', ImChatController);