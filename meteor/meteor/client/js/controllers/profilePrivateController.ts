/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
interface IProfilePrivateScope extends IHFScope {
	model: IHFUser
	avatars: ng.meteor.AngularMeteorCollection<any>;
	currentAvatar: any;//ng.meteor.AngularMeteorObject<any>;
	// isAvatarLoaded: boolean;
	cover: any;
	covers: any;
}

class ProfilePrivateController {
	static $inject = [
		'$scope',
		'temp',
		'user',
		'$meteor',
		'$timeout',
		'$log',
		'auth',
		'$q'];
	constructor(
		private $scope: IProfilePrivateScope,
		private temp: TempService,
		private user: IHFUser,
		private $meteor: IHFMeteorService,
		private $timeout: ng.ITimeoutService,
		private $log: ng.ILogService,
		private auth: any,
		private $q: ng.IQService
	) {
		$scope.model = user;
		// $meteor.autorun($scope, () => {
		$scope.$watch('currentUser', (n, o) => {
			if (!n) {
				return;
			}
			$scope.$meteorSubscribe(pubsubNames.images.avatars.current, auth.profile.id).then(() => {
				this.$scope.currentAvatar = this.$scope.$meteorObject(UserAvatars, { userId: $scope.model._id }, false);
			});
		})
		$scope.$meteorSubscribe(pubsubNames.images.cover.current, auth.profile.id).then(
			() => {
				this.$scope.cover = this.$scope.$meteorObject(UserCovers, { userId: auth.profile.id }, false);
			},
			(error) => {
				this.$log.debug('get cover', error);
			});
	}
	// private isAvatarLoaded: boolean;
	
	private openCropModal(files, cropType: CropType) {
		var dfd = this.$q.defer<string>()
		if (files.length > 0) {
			var reader = new FileReader();
			reader.onload = (e: any) => {
				this.temp.openCrop(e.target.result, cropType).result.then((data) => {
					if (data) {
						dfd.resolve(data);
					} else {
						dfd.reject('no data');
					}
				});
			};
			reader.readAsDataURL(files[0]);
		} else {
			dfd.reject('no files');
		}
		return dfd.promise;
	}
	// private isLoadingAvatar = false;
	cropAvatar(files) {
		return this.openCropModal(files, CropType.avatar).then((data) => {
			return this.uploadAvatar(data);
		}).catch((reason) => {
			this.$log.log('crop avatar err', reason);
			if (files && (reason == "no data")) {
				return this.uploadAvatar(files);
			}
		});
    };

	cropCover(files) {
		return this.openCropModal(files, CropType.cover).then((data) => {
			return this.uploadCover(data);
		}).catch((reason) => {
			this.$log.debug("crop cover err", reason);
			if (files && (reason == "no data")) {
				return this.uploadCover(files);
			}
		})
	}

	uploadAvatar(file) {
		this.$log.debug('uploading for user', this.auth.profile.id);
		var newAvatar = new FS.File(file);
		newAvatar['userId'] = this.$scope.model._id;
		this.$log.debug('got newAvatar', newAvatar);
		this.$scope.avatars = this.$scope.$meteorCollectionFS(UserAvatars, false)
		this.$log.debug('got CollectionAvatars', this.$scope.avatars);
		return this.$scope.avatars.save(newAvatar)['then']((result) => {
			this.$log.debug('saves newAvatar. Result:', result)
			this.$log.debug('calling method ' + methodNames.profile.setCurrentAvatar);
			return this.$meteor.call(methodNames.profile.setCurrentAvatar, result[0]._id._id).then((result) => {
				this.$log.debug('method ' + methodNames.profile.setCurrentAvatar + ' result:', result);
			});
		}).catch((err) => {
			this.$log.error(err);
		});
	}

	uploadCover(file) {
		var newCover = new FS.File(file);
		newCover.userId = this.$scope.model._id;
		this.$scope.covers = this.$scope.$meteorCollectionFS(UserCovers, false)
		this.$scope.covers.save(newCover).then((result) => {
			this.$scope.cover = result[0]._id;
			this.$meteor.call(methodNames.profile.setCurrentCover, result[0]._id._id).then((result) => {
				this.$log.debug('method ' + methodNames.profile.setCurrentCover + ' result:', result);
			});
		}).catch((err) => {
			this.$log.error(err);
		});
	}

}
angular.module('hf-client').controller('profilePrivateController', ProfilePrivateController);