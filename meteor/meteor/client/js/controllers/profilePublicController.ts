/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
interface IProfilePublicScope extends IHFScope {
	model: IHFUser,
	userId: string,
	$state: ng.ui.IStateService,
	cover: any;
}

//declare var moment: any;

class ProfilePublicController {
	static $inject = ['$stateParams', '$state', '$scope', '$log', '$meteor', 'auth'];
	constructor(
		private $stateParams: angular.ui.IStateParamsService,
		private $state: ng.ui.IStateService,
		private $scope: IProfilePublicScope,
		private $log: ng.ILogService,
		private $meteor: IHFMeteorService,
		private auth: any
	) {
		$scope.userId = $stateParams['userId'];
		// $meteor.autorun($scope, () => {
		// if ($scope.getReactively('currentUser') && $scope.getReactively('currentUser')['_id'] != $scope.userId) {
		// if (auth && auth.profile && auth.profile.id != $scope.userId) {
		// 	var subscribeUnsubscribe = function(subscribed) {
		// 		return function(userId) {
		// 			$meteor.call(subscribed ?
		// 				methodNames.subscriptions.unsubscribe :
		// 				methodNames.subscriptions.subscribe,
		// 				$scope.userId)
		// 				.then((subscriptionId) => {
		// 					if (subscriptionId) {
		// 						$scope['subscribed'] = !subscribed;
		// 						$scope['subscr'] = subscribeUnsubscribe(!subscribed);
		// 					}
		// 				})
		// 		}
		// 	}
		// 	$meteor.call<boolean>(methodNames.subscriptions.imSubscribed, $scope.userId)
		// 		.then((subscribed) => {
		// 			this.$log
		// 				.debug(auth.profile.id + 'is ' + (subscribed ? 'subscribed on' : 'NOT subscribed on ') + $scope.userId);
		// 			$scope['subscribed'] = subscribed;
		// 			$scope['subscr'] = subscribeUnsubscribe(subscribed)
		// 		}).catch((err) => {
		// 			this.$log.error(err);
		// 		});
		// }
		// } else {
		// 	this.$log.debug("it's me! " + $scope.getReactively('currentUser')['_id'] + ' = ' + $scope.userId);
		// }
		// });
		$scope.$state = $state;
		$scope.$meteorSubscribe(pubsubNames.users.userById, this.$scope.userId).then((sh) => {
			$scope.model = $scope.$meteorObject<IHFUser>(HFUsers, this.$scope.userId).getRawObject();
			if ($scope.model.profile &&
				$scope.model.profile.bornOn) {
				$scope.model.profile['bornOnMoment'] = moment.utc($scope.model.profile.bornOn);
			}
		});
		$scope.$meteorSubscribe(pubsubNames.images.cover.current, $scope.userId).then(
			() => {
				this.$scope.cover = this.$scope.$meteorObject(UserCovers, { userId: $scope.userId }, false);
			},
			(error) => {
				this.$log.debug('get cover', error);
			});
	}
	startChat(userId) {
		this.$meteor.call(methodNames.chats.create, userId).then(
			(chatId) => {
				this.$state.go('private.im.chat', { id: chatId });
			},
			(error) => {
				console.log(error);
			});
	}
}
angular.module('hf-client').controller('profilePublicController', ProfilePublicController);