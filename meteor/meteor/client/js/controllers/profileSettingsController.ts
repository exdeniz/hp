/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../../../lib/ImagesCollection.ts' />
/// <reference path='../../../shared/date.ts' />
/// <reference path='../../../shared/sharedEnums.ts' />

interface IProfileSettingsScope extends IHFScope {
	model: IHFUser,
	profile: IHFUserProfile;
	employers: ng.meteor.AngularMeteorCollection<IHFEmployer>;
	employers_: Mongo.Collection<IHFEmployer>;
	institutions_: Mongo.Collection<IHFInstitution>
	employerSubs: string;
	institutionSubs: string;
	newEmployers: any;
	newInstitutions: any;
	asyncCities: any;
	asyncInstitutions: any;
	asyncUsers: ng.meteor.AngularMeteorCollection<IHFUser>;
	genders: any;
	visibilityOptions: any;
	maritalStatuses: any;
	dates: any;
	months: any;
	years: any;
	bornOnDate: any;
	bornOn: any;
	institutionTypes: any;
	
	findCity: (term: string) => void;
	findInstitution: (term: string, institutionType: InstitutionTypes) => void;
	findEmployers: (term: string) => void;
	findUsers: (term: string) => void;
	selectEmployer: (employer: IHFEmployer, job: IHFUserJob) => void;
	selectInstitution: (institution: IHFInstitution, education) => void;
	tagTransform: (name: string) => void;
	getInstitutionTypeDescription: (institutionType: any) => string;
}
declare var ymaps: any;
class ProfileSettingsController {
	static $inject = ['$scope', "$meteor", 'amMoment', 'mapApiLoad', 'auth', 'institutionConstants'];
	constructor(
		private $scope: IProfileSettingsScope,
		private $meteor: any,
		private amMoment: any,
		private mapApiLoad: any,
		private auth: any,
		private institutionConstants: any
	) {
		/**Приходит user из родительского scope */
		$scope.model = $scope.model || $scope.currentUser ;
		$scope.model = $scope.model || <IHFUser>{};
		$scope.model.profile = $scope.model.profile || <IHFUserProfile>{};
		$scope.model.profile.jobs = $scope.model.profile.jobs || [];
		$scope.model.profile.educations = $scope.model.profile.educations || [];
		

		$scope.genders = Genders;

		$scope.visibilityOptions = [
			{ name: 'Доступно всем', value: 'all' },
			{ name: 'Доступно мне', value: 'me' },
			{ name: 'Доступно подписчикам', value: 'subscribers' },
			{ name: 'Доступно семье', value: 'family' }
		];

		//$scope.ddSelectSelected = $scope.visibilityOptions[0];

		$scope.maritalStatuses = [
			{ name: 'женат/замужем', value: 'married' },
			{ name: 'холост/не замужем', value: 'single' }
		];

		///cities 
		$scope.asyncCities = [];
        mapApiLoad(function() {
            $scope.findCity = function(term) {
                if (term == "") return;
				// term = $scope.country + ', ' + term; 
                ymaps.geocode(term, {
                    kind: 'locality',
                    results: 20
                }).then(function(res) {
                    $scope.asyncCities = [];
                    res.geoObjects.each(function(go) {
                        go = go.properties.getAll();
                        if (go.metaDataProperty.GeocoderMetaData.kind == 'locality') {
                            $scope.asyncCities.push(go);
                        }
                    });
                });
            };
			
			$scope.findInstitution = function(term, institutionType){
				if(!term) return;
				new ymaps.control.SearchControl({
				options: {
					provider: 'yandex#search'
				}
			}).search(term).then(function(res){
				$scope.asyncInstitutions = [];
				res.geoObjects.each(function(go) {
					go = go.properties.getAll();
					console.log('go', go);
					// Школы
					if(institutionType == InstitutionTypes.school && go.categoriesText == "Общеобразовательная школа"){
						$scope.asyncInstitutions.push(go);	
					}
					// ВУЗы
					else if(institutionType == InstitutionTypes.highSchool && go.categoriesText == "ВУЗ"){
						$scope.asyncInstitutions.push(go);						
					}
				},
				(error) => {
					console.log('error', error);
				});
			});
			}
        });
		///cities end
		/// dates
		$scope.dates = DateShared.dates;
		$scope.months = DateShared.months;
		$scope.years = DateShared.years;
		///dates end
		///born
		$scope.bornOnDate = moment.utc($scope.model.profile.bornOn) || moment.utc(new Date());
		$scope.bornOn = {
			date: function(val) {
				return val != undefined ? $scope.bornOnDate.date(val) : $scope.bornOnDate.date();
			},
			month: function(val) {
				return val != undefined ? $scope.bornOnDate.month(val) : $scope.bornOnDate.month();
			},
			year: function(val) {
				return val != undefined ? $scope.bornOnDate.year(val) : $scope.bornOnDate.year();
			},
		};
		///born end
		// Geneology
		$scope.findUsers = function(term) {
			if(!term) return;
			$scope.$meteorAutorun(function() {
				$scope.asyncUsers = HFUsersIndex.search({'profile.firstName':term, 'profile.lastName' : term, 'profile.middleName' : term},{
					limit: 10,
  					props: {
						  excludeIds: [$scope.currentUser._id]
					  }
				}).fetch();
			});
		}
		$scope.model.profile.geneology = {
			maritalStatus: $scope.maritalStatuses[0],
			childrens: [{ name: '' }],
			parents: [{ name: '' }]
		};
		
		// Employers
		$scope.employers_ = Employers;
		$scope.employerSubs = pubsubNames.employers.employerById;
		$scope.newEmployers = {};
		
		$scope.findEmployers = function(term) {
			$scope.$meteorAutorun(function() {
				$scope.employers = HFEmployersIndex.search({ name: term }).fetch();
			});
		}

		$scope.tagTransform = function(name) {
			if(!name) return;
			return { name: name };
		};
		
		$scope.selectEmployer = function(employer: IHFEmployer, job){
			if(!job.employerId && employer){
				$scope.newEmployers[job.$$hashKey] = employer;
			}
		}
		
		// Educations
		$scope.institutions_ = Institutions;
		$scope.institutionSubs = pubsubNames.institutions.institutionById;
		$scope.newInstitutions = {};
		$scope.institutionTypes = institutionConstants().institutionTypes;
		
		$scope.selectInstitution = function(institution: any, education){
			if(institution){
				$scope.newInstitutions[education.$$hashKey] = <IHFInstitution>{
					name: institution.name,
					yandexData: institution.responseMetaData
				};
			}
		}
		
		// Contacts
		$scope.model.profile.contacts = {
			phone: '+7 903 800 01 23',
			email: 'email@gmail.com',
			facebook: 'facebook1221323',
			site: 'www.super.me'
		};

		$scope.profile = angular.copy($scope.model.profile);
	}
	
	setProfile (form: ng.IFormController) {
			if (form.$invalid) return;
			this.$scope.profile = angular.copy(this.$scope.model.profile);
			this.$scope.model.profile.bornOn = this.$scope.bornOnDate.toDate();
			HFUsers.update(this.$scope.currentUser._id, 
			{ $set: { 
				'profile.firstName': this.$scope.model.profile.firstName,
				'profile.lastName': this.$scope.model.profile.lastName,
				'profile.middleName': this.$scope.model.profile.middleName,
				'profile.madenName': this.$scope.model.profile.madenName,
				'profile.bornOn': this.$scope.model.profile.bornOn,
				'profile.gender': this.$scope.model.profile.gender,
				'profile.additionalInfo': this.$scope.model.profile.additionalInfo,
				'profile.additionalInfo2': this.$scope.model.profile.additionalInfo2,
				'profile.address': this.$scope.model.profile.address
		 } });
		};
		
	setJobs (form: ng.IFormController) {
		_.map(this.$scope.model.profile.jobs, (job: any) => {
			var newEmployer = this.$scope.newEmployers[job.$$hashKey];
			if(newEmployer){
				var employerId = Employers.insert(newEmployer);
				if(employerId){
					job.employerId = employerId;
				}
			}
		});
		this.$scope.profile.jobs = angular.copy(this.$scope.model.profile.jobs);
		HFUsers.update(this.$scope.currentUser._id, { $set: { 'profile.jobs': this.$scope.profile.jobs } });
		this.$scope.newEmployers = {};
	};
	
	setEducations (form: ng.IFormController) {
		_.map(this.$scope.model.profile.educations, (education: any) => {
			var newInstitution = this.$scope.newInstitutions[education.$$hashKey];
			if(newInstitution){
				var institutionId = Institutions.insert(newInstitution);
				if(institutionId){
					education.institutionId = institutionId; 
				}
			}
		})
		this.$scope.profile.educations = angular.copy(this.$scope.model.profile.educations);
		HFUsers.update(this.$scope.currentUser._id, { $set: { 'profile.educations': this.$scope.profile.educations }});
		this.$scope.newInstitutions = {};
	};

	setDefault() {
		this.$scope.model.profile = angular.copy(this.$scope.profile);
		this.$scope.bornOnDate = moment.utc(this.$scope.model.profile.bornOn) || moment.utc(new Date());
	}

	activateProfile() {
		//return this.auth.resendActivation();	
	}

	checkByNow(item) {
		if (item.now) {
			item.finishedAt = undefined;
		}
	}
	
	getInstitutionTypeDescription (institutionType){
		var type =  _.find(this.$scope.institutionTypes, (t: any) => {
			return t.value == institutionType
		});
		return type ? type.name : '';
	}
}

angular.module('hf-client').controller('profileSettingsController', ProfileSettingsController);

class IninstitutionConstants {
	static values(){
		return {
			institutionTypes: [
					{ name: 'ВУЗ', value: InstitutionTypes.highSchool },
					{ name: 'Школа', value: InstitutionTypes.school },
				]
			}
	}	
}

angular.module('hf-client').constant('institutionConstants', IninstitutionConstants.values);