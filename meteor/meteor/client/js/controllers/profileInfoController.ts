/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../../../shared/sharedEnums.ts' />
interface IProfileInfoScope extends IHFScope {
	model: IHFUser,
	userId: string,
	genders: any,
	employers_: Mongo.Collection<IHFEmployer>;
	institutions_: Mongo.Collection<IHFInstitution>
	employerSubs: string;
	institutionSubs: string;
}

class ProfileInfoController {
	static $inject = ['$stateParams', '$scope', '$log', 'institutionConstants'];
	constructor(
		private $stateParams: angular.ui.IStateParamsService,
		private $scope: IProfileInfoScope,
		private $log: ng.ILogService,
		private institutionConstants: any
	) {
		$log.debug('profileInfoController');
		$scope.genders = Genders;
		$scope.employers_ = Employers;
		$scope.employerSubs = pubsubNames.employers.employerById;
		$scope.institutions_ = Institutions;
		$scope.institutionSubs = pubsubNames.institutions.institutionById;
	}

	getInstitutionTypeDescription(institutionType) {
		var type = _.find(this.institutionConstants().institutionTypes, (t: any) => {
			return t.value == institutionType
		});
		return type ? type.name : '';
	}
}
angular.module('hf-client').controller('profileInfoController', ProfileInfoController);