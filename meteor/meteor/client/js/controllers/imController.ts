/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../interfaces/Chat.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../interfaces/Scopes.d.ts' />
/// <reference path='../../../lib/ChatsCollection.ts' />
/// <reference path='../../../lib/PublicationNames.ts' />
interface IImScope extends IHFScope {
	chats: ng.meteor.AngularMeteorCollection<IHFChat>;
	messages: ng.meteor.AngularMeteorCollection<IHFMessage>;
	interlocutors: ng.meteor.AngularMeteorCollection<IHFUser>;
	interlocutorIds: string[];
	usersCollection: any;
	userSubsName: string;
	isSubscribing: boolean;
	$state: any;
	messangerUserScrollOptions: any;
	messangerChatMessagesScrollOptions: any;
}
class ImChatsController {
	static $inject = ['$scope', '$meteor', '$state', '$timeout', 'auth'];
	constructor(
		private $scope: IImScope,
		private $meteor: IHFMeteorService,
		private $state: IStatetable,
		private $timeout: ng.ITimeoutService,
		private auth: any
	) {
		$scope.isSubscribing = true;
		$scope.$state = $state;
		$scope.usersCollection = Meteor.users;
		$scope.userSubsName = pubsubNames.users.userById;
		
		$meteor.autorun($scope, () => {
			$scope.$meteorSubscribe(pubsubNames.chats.myChats).then(() => {
				$scope.isSubscribing = false;
				$scope.chats = $scope.$meteorCollection<IHFChat>(() => {
					var chts = Chats.find({lastMessageId : {$exists : true}}, {
						sort: { lastMessageAt: -1 },
					});
					return chts;
				}, false);
			}).then(() => {
				// $scope.interlocutorIds = _.map($scope.chats, (chat) => {
				// 	var userId = this.getInterlocutorId(chat);
				// 	if (userId) return userId;
				// });
				// $scope.$meteorSubscribe(pubsubNames.users.usersById, $scope.interlocutorIds).then(() => {
				// 	$scope.interlocutors = $scope.$meteorCollection<IHFUser>(() => {
				// 		var users = HFUsers.find({});
				// 		return users;
				// 	}, false);
				// });
			});
			$scope.$meteorSubscribe(pubsubNames.chats.lastMessages,
				Chats.find().map((c) => c.lastMessageId)).then(() => {
					$scope.chats.forEach((ch) => {
						ch['lastMessage'] = Messages.findOne({
							_id: ch.lastMessageId
						});
					});
				});
		});

		var messangerUserScrollOptions = {
			height: '100%',
			railVisible: true,
			railColor: '#fff',
			railOpacity: 1,
			color: '#dde1e5',
			size: '12px',
			alwaysVisible: true,
			borderRadius: '0px',
			railBorderRadius: '0px'
		};


		$scope.messangerUserScrollOptions = function() { return messangerUserScrollOptions };
	}
	createChat(title, message, userId) {
		this.$meteor.call<boolean>(methodNames.chats.create, title, message, userId).then((b) => {
			// console.log('created?', b);
		});
	}
	menuClick(e) {
		e.stopPropagation();
	}
	getInterlocutorId(chat: IHFChat) {
		if (chat && chat.type == ChatTypes.Dialog) {
			return _.find(chat.users, (user) => {
				return user.id != this.auth.profile.id;
			}).id;

		}
	}
}

angular.module('hf-client').controller('imController', ImChatsController);