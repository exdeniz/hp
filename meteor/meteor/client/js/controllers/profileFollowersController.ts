/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../../../lib/SubscriptionsCollections.ts' />

interface IProfileFollowersScope extends IHFScope {
	model: IHFUser,
	userId: string,
	$state: ng.ui.IStateService
	follows: ng.meteor.AngularMeteorCollection<IHFSubscription>;
	users: IHFUser[];//ng.meteor.AngularMeteorCollection<IHFUser>;
	usersCollection: any;
	userSubsName: string;
	getFollowerUser(userId:string):IHFUser;
	IsSubscribedOn(userId:string):ng.IPromise<boolean>;
}
class ProfileFollowersController {
	static $inject = ['$state', '$scope', '$log', '$meteor', '$stateParams'];

	constructor(
		private $state: ng.ui.IStateService,
		private $scope: IProfileFollowersScope,
		private $log: ng.ILogService,
		private $meteor: ng.meteor.IMeteorService,
		private $stateParams: ng.ui.IStateParamsService
	) {
		$scope.usersCollection = Meteor.users;
		$scope.userSubsName = pubsubNames.users.userById;
		
		$meteor.autorun($scope, () => {
			$scope.$meteorSubscribe(pubsubNames.subscriptions.followers, $stateParams['userId']).then(() => {
				$scope.follows = $scope.$meteorCollection(Subscriptions, true, Subscriptions);
				var ids = Subscriptions.find({ userId: $stateParams['userId'] })
						.map((subscription) => {
							return subscription.userId;
						});
						$log.debug('ids', ids);
				// $scope.$meteorSubscribe(pubsubNames.users.usersById,ids ).then(() => {
				// 	$log.debug('resubscribe', ids);
				// 		$scope.users = HFUsers.find({ _id: { $in: 
				// 				Subscriptions.find({ folowToUserId: $stateParams['userId'] })
				// 				.map((subscription) => {
				// 					return subscription.userId;
				// 				}) }
				// 			 }).fetch();
				// 			 $log.debug('followers users',$scope.users);
				// 	});
			});
		});
	 }
	//  getFollowerUser(userId:string){
	// 	 debugger;
	// 	 this.$log.debug('getting ' + userId);
	// 	 var user = _.find(this.$scope.users,(u:IHFUser) => u._id == userId);
	// 	 this.$log.debug('got ', user);
	// 	 return user;
	//  }
	 setViewed(f:IHFSubscription){
		 this.$log.debug('set viwed ', f);
		 if(this.$scope.currentUser._id == f.folowToUserId &&  this.$scope.currentUser._id == this.$stateParams['userId']) {
			 Subscriptions.update(f['_id'], {
					 $set : { seenByFolowToUser : true }
			 });
		 }
	 }
}

angular.module('hf-client').controller('followersController', ProfileFollowersController);