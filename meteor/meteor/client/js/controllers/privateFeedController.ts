/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../interfaces/Chat.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../interfaces/Scopes.d.ts' />
/// <reference path='../../../lib/ChatsCollection.ts' />
/// <reference path='../../../lib/PublicationNames.ts' />

/**
 * PrivateFeedController
 */
class PrivateFeedController {
	static $inject = ['$scope', '$meteor', '$state', '$timeout', 'auth', 'themeService'];
	constructor(
		private $scope: any,
		private $meteor: IHFMeteorService,
		private $state: ng.ui.IStateService,
		private $timeout: ng.ITimeoutService,
		private auth: any,
		private themeService: ThemeService) {
		this.loadData();
	}

	loadData() {
		this.themeService.init().then((result) => {
			
		});
	}

	openModal() {
		this.themeService.openModal(true).then(
			(theme: Theme) => {
				this.$scope.themeId = theme._id;
			},
			(error) => {console.log(error)}
		);
	}
}

angular.module('hf-client').controller('privateFeedController', PrivateFeedController);
