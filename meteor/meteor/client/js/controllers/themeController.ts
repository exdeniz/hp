/// <reference path='../services/themeService.ts' />
/// <reference path='../../../interfaces/Theme.d.ts' />
/// <reference path='../../../lib/ThemesCollection.ts' />
/// <reference path="../../../typings/tsd.d.ts" />

interface IThemeScope extends IHFScope {
	rootThemes: Theme[];
	subscriptions: ThemeSubscription[];
	themes: _.Dictionary<Theme>;
	searchChildrenResult: Theme[];

	loaded: boolean;
	loadingPromise: ng.IPromise<any>;
	pendingPromise: ng.IPromise<any>;
}

class ThemeController {
	static $inject = ['$scope', 'themeService', '$q', '$modal', '$modalStack', '$meteor'];

	constructor(
		private $scope: IThemeScope,
		private themeService: ThemeService,
		private $q: ng.IQService,
		private $modal: angular.ui.bootstrap.IModalService,
		private $modalStack: angular.ui.bootstrap.IModalStackService,
		private $meteor: ng.meteor.IMeteorService) {
		this.loadData();
	}

	loadData() {
		this.themeService.init().then((result) => {
			this.$scope.rootThemes = this.themeService.data.rootThemes;
			this.$scope.themes = this.themeService.data.themes;
			this.$scope.subscriptions = this.themeService.data.subscriptions;
		});
	}

	// toggleSubscription(theme: Theme, includeChildren: boolean) {
	// 	var dfd = this.$q.defer<Theme[]>();
	// 	this.$scope.pendingPromise = dfd.promise;
	// 	this.themeService.toggleSubscription(theme, includeChildren).then(r => dfd.resolve(r), e => dfd.reject(e));
	// 	return dfd.promise;
	// }

	filterSubscriptionsByRoot(rt: Theme): ThemeSubscription[] {
		var sub = _.filter(this.$scope.subscriptions, s => this.$scope.themes[s.themeId].rootId == rt._id);
		return sub;
	}

	openModal(rootTheme: Theme, parentTheme?: Theme) {
		this.themeService.openModal(false, rootTheme, parentTheme);
	}

	subscribe(themeId: string) {
		this.themeService.subscribe(themeId);
	}

	unsubscribe(themeId: string) {
		this.themeService.unsubscribe(themeId);
	}
}

angular.module('hf-client').controller('themeController', ThemeController);