/// <reference path='../services/tempService.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
/// <reference path='../../../lib/UserCollection.ts' />
/// <reference path='../../../lib/SubscriptionsCollections.ts' />

interface IProfileSubscriptionsScope extends IHFScope {
	model: IHFUser,
	userId: string,
	$state: ng.ui.IStateService
	subscriptions: ng.meteor.AngularMeteorCollection<IHFSubscription>;
	users: ng.meteor.AngularMeteorCollection<IHFUser>;
}

class ProfileSubscriptionsController {
	static $inject = ['$state', '$scope', '$log', '$meteor', '$stateParams'];

	constructor(
		private $state: ng.ui.IStateService,
		private $scope: IProfileSubscriptionsScope,
		private $log: ng.ILogService,
		private $meteor: ng.meteor.IMeteorService,
		private $stateParams: ng.ui.IStateParamsService
	) {
		$scope.$state = $state;
		$scope.$watch('currentUser', (n, o) => {
			if (!n) {
				return;
			}
			$meteor.autorun($scope, () => {
				$scope.$meteorSubscribe(pubsubNames.subscriptions.subscribed, $stateParams['userId']).then(() => {
					$scope.subscriptions = $scope.$meteorCollection(Subscriptions, false, Subscriptions);
					
					var ids = Subscriptions.find({ userId: $stateParams['userId'] })
						.map((subscription) => {
							return subscription.folowToUserId;
						});
					$scope.$meteorSubscribe(pubsubNames.users.usersById,ids ).then(() => {
						$scope.users = $scope.$meteorCollection<IHFUser>(() => {
							// $log.debug('looking for users', HFUsers.find({ _id: { $in: ids } }).fetch())
							return HFUsers.find({ _id: { $in: 
								Subscriptions.find({ userId: $stateParams['userId'] })
								.map((subscription) => {
									return subscription.folowToUserId;
								}) } });
						}, false);
					});
				});

				$log.debug('currentUser, ', n, $scope.currentUser)
			});
		})
	}
}

angular.module('hf-client').controller('subscriptionsController', ProfileSubscriptionsController);