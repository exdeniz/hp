/// <reference path='../services/tempService.ts' />
/// <reference path='../../../shared/socialProviders.ts' />
/// <reference path='../../../interfaces/User.d.ts' />
interface ISignupModel {
	firstName: string;
	secondName: string;
	gender?: Genders;
	email: string;
	password: string;
	passwordConfirm: string;
}
interface ISignUpScope extends IHFScope {
	model: ISignupModel;
	genders: any; 
}

class SignupController {
	//mapApiLoad
	static $inject = ['$scope', '$q', '$log', 'authWrapper', 'auth', '$meteor', 'temp', '$state', 'ui'];
	constructor(
		private $scope: ISignUpScope,
		private $q: angular.IQService,
		private $log: angular.ILogService,
		private authWrapper: AuthWrapper,
		private auth: any,
		private $meteor: IHFMeteorService,
		private temp: TempService,
		private $state: ng.ui.IStateService,
		private ui: UiService
	) {
		$scope.genders = Genders;
	}
	public isSigninUp = false;
	signUp(form: ng.IFormController, email: string, password: string) {
		this.$log.debug('signUp', email);

		if (form.$invalid) {
			this.$log.error('signUp', 'formIsInvalid', form);
			form.$setDirty();
			return;
		}
		this.isSigninUp = true;
		this.$log.debug('signUp', 'form valid');
		this.authWrapper.signUp({ email, password })
			.then((userdata) => {
				this.$log.debug(userdata);
				this.$meteor.waitForUser().then((user) => {
					console.log('user is', user);
					var users = this.$scope.$meteorCollection<IHFUser>(HFUsers, false);
					var model = this.$scope.model;
					user.profile = user.profile || <IHFUserProfile>{};
					user.profile.firstName = model.firstName;
					user.profile.lastName = model.secondName;
					user.profile.gender = model.gender;
					user.profile.status = EntityStatuses.Active;
					this.$log.debug('saveing user start');
					HFUsers.update(user._id, { $set: { profile: user.profile } })
					this.$log.debug('saveing user end');
					this.isSigninUp = false;
					this.temp.openCompleteSignUp(user).catch(() => {
						this.ui.ShowSideMenu();
						this.$state.go('public.profile.info', { userId: user._id });
					})
				});
				///$meteor.call('updateUserData',userdata);
			})
	}
	socialSignUp(provider: SocialProviders) {
		this.$log.debug('social provider', provider);
		this.auth.oauth.popup(provider)
			.then((si) => {
				///Ищем пользователя у нас в бд.
				return this.auth.socialLogin(si).then(function() {
					//Пользователь найден и залогинен
					//state.go можно использовать
					location.reload();
				}).catch(() => {
					///пользователь новый, Получаем информацию о пользователе
					return this.auth.oauth.me(si)
						.then((me) => {
							//Регистрируем
							return this.auth.user.signup(me)
								.then(() => {
									//Входим
									this.auth.socialLogin(me)
										.then(() => {
											//выставить аватарку, задеплоить данные о пользователе.///$meteor.call('updateUserData',userdata);
											//потом открыть попап
											this.$meteor.waitForUser().then((user) => {
												this.temp.openCompleteSignUp(user).catch(() => {
													this.$state.go('public.profile.info', { userId: user._id });
												});
											});
										})
										.catch(() => {
											this.$log.error('ошибка входа');
										});
								}, () => {
									this.$log.error("Email привязанный к этой записи уже используется")
								});
						})
				})
			}, () => {
				this.$log.error("Не удалось открыть всплывающее окно");
			});
    }
}
angular.module('hf-client').controller('signupController', SignupController);