/// <reference path='../interfaces/Scopes.d.ts' />
/// <reference path='../../../interfaces/Counts.d.ts' />

interface ISubscriptionsBarScope extends IHFScope {
	$state: ng.ui.IStateService
}

class SubscriptionsBarController {
	static $inject = ['$state', '$scope', '$log', '$meteor', '$stateParams'];

	constructor(
		private $state: ng.ui.IStateService,
		private $scope: ISubscriptionsBarScope,
		private $log: ng.ILogService,
		private $meteor: ng.meteor.IMeteorService,
		private $stateParams: ng.ui.IStateParamsService
	) {
		$scope.$meteorSubscribe(pubsubNames.subscriptions.followersCount, $stateParams['userId'])
			.then(() => {
				$scope['subscribersCount'] = $scope.$meteorObject(Counts, pubsubNames.subscriptions.followersCount, false);
			});

		$scope.$meteorSubscribe(pubsubNames.subscriptions.subscriptionsCount, $stateParams['userId'])
			.then(() => {
				$scope['subscriptionsCount'] = $scope.$meteorObject(Counts, pubsubNames.subscriptions.subscriptionsCount, false);
			});
	}
}

angular.module('hf-client').controller('subsBarController', SubscriptionsBarController);