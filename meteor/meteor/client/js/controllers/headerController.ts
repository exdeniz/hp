/// <reference path='../services/tempService.ts' />
class HeaderController {
	static $inject = ['$scope', '$q', '$log', 'temp', 'auth', '$state', 'ui'];
	constructor(
		private $scope: IHFScope,
		private $q: angular.IQService,
		private $log: angular.ILogService,
		private temp: TempService,
		private auth: any,
		private $state: ng.ui.IStateService,
		private ui: UiService
	) { }
	login() {
		if (this.$scope.currentUser || this.$scope.loggingIn) {
			return;
		}
		this.temp.openLogin();
	}
	logout() {
		this.$log.debug('logout');
		this.auth.signout();
		this.ui.HideSideMenu();
		if (this.$state.includes("private")) {
			this.$log.debug('anoni,usin private state... redirecting');
			this.$state.go('public.anonimus.registration');
		}
	}
}

angular.module('hf-client').controller('headerController', HeaderController);