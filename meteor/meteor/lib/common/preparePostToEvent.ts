/// <reference path="../EventsCollection.ts" />
/**Преобразует post клиента в IHFevent общий */
declare var ConvertPostToEventBeforeSaving: (crrentUser: string, post: any, groupId?: string) => IHFEvent
/**Преобразует post клиента в IHFevent общий */
ConvertPostToEventBeforeSaving = (crrentUser: string, post: any, groupId?: string) => {
	var event: IHFEvent = {
		likesCount : 0,
		themeId: post.themeId,
		context: post.selectedContext.value,
		createdAt: new Date(),
		createdBy: crrentUser,
		createdByGroup: groupId,
		dates: {
			from: post.dateRange.startDateAt,
			to: post.dateRange.endDateAt
		},
		linkedEventIds: post.linkedEventId ? [post.linkedEventId] : [],
		members: post.users,
		status: EntityStatuses.Active,
		tags: [],
		nestedEventIds: post.nestedEventId ? [post.nestedEventId] : [],
		content: <IHFArticleContent>{
			text: post.text,
			title: post.title,
			filesId: post.filesId,
			photoId: ""
		}
	};
	return event;
}

// возвращает все хэштэги для текста
declare var ScanTextForHashtags: (text: string) => string[];
ScanTextForHashtags = (text: string) => {
	return text.toLowerCase().match(/#(?:\[[^\]]+\]|\S+)/g) || []
}