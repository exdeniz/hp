declare var pubsubNames: {
	users: {
		usersById: string,
		all: string;
		userById: string;
	},
	themes: {
		all: string,
		subscriptions: string
	}
	chats: {
		chatById: string;
		myChats: string;
		authors: string;
		lastMessages: string;
		messages: string;
		messagesNewCount: string;
	},
	subscriptions: {
		subscribed: string;
		followers: string;
		followersNewCount: string;
		followersCount: string;
		subscriptionsCount: string;
	},
	images: {
		avatars: {
			current: string;
		},
		cover: {
			current: string;
		};
	},
	employers: {
		employersById: string;
		employerById: string;
	},
	institutions: {
		institutionById: string;
	}
}
declare var methodNames: {
	chats: {
		create: string;
		sendMessage: string;
		getNewMessagesCount: string;
	},
	subscriptions: {
		imSubscribed: string;
		subscribe: string;
		unsubscribe: string;
	}
	profile: {
		setCurrentCover: string;
		setCurrentAvatar: string;
		setCover: string;
	},
	events : {
		addEvents : string;
	},
	themes: {
		subscribe: string,
		unsubscribe: string,
		create: string;
	}
}
pubsubNames = {
	users: {
		userById: "users-by-id",
		usersById: "users.byId",
		all: "users.all"
	},
	themes: {
		all: 'themes.all',
		subscriptions: 'themes.subscriptions.byUserId'
	},
	chats: {
		chatById: "chat.byId",
		myChats: "chats.my",
		authors: "chats.authors",
		lastMessages: 'chats.lastMessages',
		messages: 'chat.messages',
		messagesNewCount: 'chat.messages.new.count'
	},
	subscriptions: {
		subscribed: 'subscriptions.subscribed',
		followers: 'subscriptions.folowers',
		followersNewCount: 'subscriptions.folowers.new.count',
		followersCount: 'subscriptions.folowers.count',
		subscriptionsCount: 'subscriptions.subscribed.count'
	},
	images: {
		avatars: {
			current: 'avatars.my'
		},
		cover: {
			current: 'cover.my'
		}
	},
	employers: {
		employerById: 'employer.byId',
		employersById: 'employers.byIds'
	},
	institutions: {
		institutionById: 'institution.ById'
	}
}
methodNames = {
	chats: {
		create: 'chat.create',
		sendMessage: 'chat.sendMessage',
		getNewMessagesCount: 'chat.getNewMessagesCount'
	},
	subscriptions: {
		imSubscribed: 'subscriptions.imSubscribed',
		subscribe: 'subscriptions.subscribe',
		unsubscribe: 'subscriptions.unsubscribe'
	},
	profile: {
		setCurrentCover: "profile.setCurrentCover",
		setCurrentAvatar: "profile.setCurrentAvatar",
		setCover: "profile.setCover"
	},
	events : {
		addEvents : "events.addEvent"
	},
	themes: {
		subscribe: 'themes.subscribe',
		unsubscribe: 'themes.unsubscribe',
		create: 'themes.create'
	}
}