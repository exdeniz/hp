///<reference path='../typings/tsd.d.ts' />
///<reference path='../interfaces/Chat.d.ts' />
declare var Chats: Mongo.Collection<IHFChat>;
declare var Messages: Mongo.Collection<IHFMessage>;
Chats = new Mongo.Collection<IHFChat>('hfchats');
Messages = new Mongo.Collection<IHFMessage>('hfmessages');