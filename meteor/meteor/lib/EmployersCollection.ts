///<reference path='../typings/tsd.d.ts' />
///<reference path='../interfaces/Employer.d.ts' />
declare var Employers: Mongo.Collection<IHFEmployer>;
Employers = new Mongo.Collection<IHFEmployer>('employers');
declare var HFEmployersIndex: any;
declare var EasySearch: any;
// if(Meteor.isServer){
// 	Employers._ensureIndex('name', {unique: 1});	
// }
HFEmployersIndex = new EasySearch.Index({
	collection: Employers,
	fields: ['name'],
	permission: () => true,
	engine: new EasySearch.MongoDB()
	// ({
	// 	//selector: { score: "textScore" },
	// 	// selector:function(a,b,c) { 
	// 	// 	var selector = this.defaultConfiguration().selector(a,b,c);
	// 	// 	selector.score = { '$meta': "textScore" };
	// 	// 	console.log('selector', selector);
	// 	// 	return selector;  
	// 	// },
	// 	// fields: function(searchObject, options) {
	// 	// 	return ['name', 'score'];
	// 	// },
	// 	// sort: () => {return { score: { '$meta': "textScore" } }}
	// }),
});

// Employers.find(
//     { $text: {$search: searchValue} },
//     {sort: {
//         score: { $meta: "textScore" }
// 	}
//     })