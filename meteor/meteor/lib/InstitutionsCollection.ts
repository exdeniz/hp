///<reference path='../typings/tsd.d.ts' />
///<reference path='../interfaces/Institution.d.ts' />
declare var Institutions: Mongo.Collection<IHFInstitution>;
Institutions = new Mongo.Collection<IHFInstitution>('institutions');
declare var HFInstitutionsIndex: any;
declare var EasySearch: any;

HFInstitutionsIndex = new EasySearch.Index({
	collection: Institutions,
	fields: ['name'],
	permission: () => true,
	engine: new EasySearch.MongoDB()
});