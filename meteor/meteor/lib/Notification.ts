///<reference path='../typings/tsd.d.ts' />
///<reference path='PublicationNames.ts' />
var collectionName = "NOTIFICATIONS_CLIENT_ONLY_COLLECTION";
var subscriptionName = "NOTIFICATIONS_SUBSCRIPTION_NAME"
declare var Notifications: Mongo.Collection<any>;
declare var Notif: { [key: string]: any }
if (Meteor.isServer) {
	Notif = this.Notif || {};

	Meteor.publish(subscriptionName, function(notificationTypesc: string[]) {
		var userId = this.userId;
		// console.log('notif userId - ', userId);
		// console.log('notif types - ', notificationTypesc);
		if (!userId) return;

		var self = this;
		// console.log('notif self: ', self);

		_(notificationTypesc).forEach((nType) => {
			// console.log('notif initing ', nType);
			self.added(collectionName, nType, {});
		});
		// console.log('notif object - ', Notif);
		Notif[userId] = self;
		this.onStop(() => {


			_(notificationTypesc).forEach((nT) => {
				// console.log('notif stoping', nT);
				self.removed(collectionName, nT)
			});
			delete Notif[userId];
		});
	});
}
if (Meteor.isClient) {
	this.Notif = this.Notif || {}
	this.Notif['on'] = {
		'UserIsTyping': (NotificationObject) => {
			console.log("user is typing: ", NotificationObject);
		},
		'AuctionUpdated': (AuctionData) => {
			console.log('KAE set new auction data', AuctionData);
		}
	};
	Notifications = new Mongo.Collection<any>(collectionName);
	Meteor.subscribe(subscriptionName, _.keys(Notif['on']));
	Notifications.find().observeChanges({
		added: (notificationType) => {
			console.log('inited', notificationType);
		},
		changed: (notificationType, notificationObject) => {
			Notif['on'][notificationType](notificationObject);
		}
	});
}

if (Meteor.isServer) {
	Meteor.methods({
		'test_notification': function(userId, notificationType, obj: any) {
			///библиотечный код по сути
			if (Notif[userId]) {
				Notif[userId].changed(collectionName, notificationType, obj);
				return true;
			}
			return false;
		}
	});
}