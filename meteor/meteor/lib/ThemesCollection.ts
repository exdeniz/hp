///<reference path='../typings/tsd.d.ts' />
///<reference path='../interfaces/Theme.d.ts' />

declare var Themes: Mongo.Collection<Theme>
declare var ThemesSubscriptions: Mongo.Collection<ThemeSubscription>
Themes = new Mongo.Collection<Theme>('themes');
ThemesSubscriptions = new Mongo.Collection<ThemeSubscription>('themes-subsciptions');

declare var HFThemesIndex: any;
declare var EasySearch: any;
HFThemesIndex = new EasySearch.Index({
	collection: Themes,
	fields: ['name'],
	permission: () => true,
	engine: new EasySearch.MongoDB({
		//sort: () => { createdAt: -1 },
		selector: function(searchObject, options, aggregation) {
			let selector = this.defaultConfiguration().selector(searchObject, options, aggregation);
			if (options.search.props.parentId) {
				selector.parentId = options.search.props.parentId;
			}
			return selector;
		}
	}),
});