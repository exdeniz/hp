///<reference path='../typings/tsd.d.ts' />
///<reference path='PublicationNames.ts' />
declare var UserAvatars: any;
declare var UserCovers: any;
declare var FS: any;
declare var Coll: Mongo.Collection<any>;
UserAvatars = new FS.Collection("avatars", {
  stores: [
    new FS.Store.FileSystem("original", { path: "../../../../../uploads/avatars" }),
    // new FS.Store.FileSystem("thubms", { path: "/uploads/thumbs" })
    // new FS.Store.GridFS("images")
  ],
  filter: {
    allow: {
      contentTypes: ['image/*']
    }
  }
});

UserCovers = new FS.Collection("covers", {
  stores: [
    new FS.Store.FileSystem("images", { path: "../../../../../uploads/covers" }),
    // new FS.Store.FileSystem("thubms", { path: "/uploads/thumbs" })
    // new FS.Store.GridFS("images")
  ],
  filter: {
    allow: {
      contentTypes: ['image/*']
    }
  }
});

// UserAvatars.findOne({_id : "asas"}).url({store : 'thubms'});
if (Meteor.isServer) {
 
  // Meteor.methods({
  //   [methodNames.profile.updateAvatar]: function(fileId) {
  //     if (!this.userId) {
  //       throw new Meteor.Error('forbidden');
  //     }
  //     var user = <IHFUser>Meteor.users.findOne(this.userId);
  //     if (!user) {
  //       throw new Meteor.Error('forbidden');
  //     }
  //     console.log(fileId);
  //     var file = UserAvatars.findOne(fileId);
  //     console.log(file);
  //     user.profile.avatar.url = file.url();
  //     user.profile.avatar.imageId = file._id;
  //     user.profile.avatar.collectionName = 'UserAvatars';
  //     console.log(user);
  //     return file.url();
  //   }
  // })
}



// Coll = new Mongo.Collection("tempCollection");
// if (Meteor.isServer) {
//   Coll.allow({
//     insert: function(uid, doc) {
//       console.log('insert', uid);
//       return true;
//     },
//     update: function(uid, doc) {
//       console.log('update', uid);
//       return true;
//     },
//     remove: function(uid, doc) {
//       console.log('remove', uid);
//       return true;
//     }
//   });
// }