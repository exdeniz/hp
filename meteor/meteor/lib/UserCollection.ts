///<reference path='../typings/tsd.d.ts' />
///<reference path='../interfaces/User.d.ts' />
declare var HFUsers: Mongo.Collection<IHFUser>;
HFUsers = <Mongo.Collection<IHFUser>>Meteor.users;
declare var HFUsersIndex: any;
declare var EasySearch: any;
HFUsersIndex = new EasySearch.Index({
	collection: Meteor.users,
	fields: ['profile.firstName', 'profile.middleName', 'emails', 'profile.lastName'],
	allowedFields: ['profile.firstName', 'profile.middleName', 'profile.lastName'],
	permission: () => true,
	engine: new EasySearch.MongoDB({
		sort: () => { createdAt: -1 },
		// fields: () => ['profile', 'emails'],
		selector: function(searchObject, options, aggregation) {
			let selector = this.defaultConfiguration().selector(searchObject, options, aggregation);
			if (options.search.props.excludeIds) {
				selector._id = {$nin : options.search.props.excludeIds}
				//selector._id = options.search.props.brand;
			}

			return selector;
		},
		selectorPerField: function(field: string, searchString: string) {
			// console.log('searching field', field);
			// console.log('searching string', searchString);
			var selector;
			if ('emails' === field) {
				selector = {
					emails: {
						$elemMatch: {
							address: { '$regex': '.*' + searchString + '.*', '$options': 'igm' }
						}
					}
				};
				console.log('selecotor:', _.pairs(selector), _.pairs(selector.emails.$elemMatch));
			} else {
				selector = this.defaultConfiguration().selectorPerField(field, searchString)
				console.log('selecotor:', _.pairs(selector));
			}

			return selector;
		}
	}),
});