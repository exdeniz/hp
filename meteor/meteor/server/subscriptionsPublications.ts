/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../lib/SubscriptionsCollections.ts' />
/// <reference path='../interfaces/Chat.d.ts' />
/// <reference path='../interfaces/User.d.ts' />
/// <reference path='../lib/PublicationNames.ts' />
/// <reference path='../lib/ChatsCollection.ts' />
/// <reference path='../shared/sharedEnums.ts' />
/// <reference path='../interfaces/Counts.d.ts' />


Subscriptions.allow({
	insert: (userId, doc) => {
		return userId == doc.userId && !Subscriptions.findOne({ userId: userId, folowToUserId: doc.folowToUserId });
	},
	update: (userId, doc, fieldNames, modifier) => {
		if (userId == doc.folowToUserId && fieldNames.length == 1 && fieldNames[0] == 'seenByFolowToUser') {
			console.log("modifier['$set']['seenByFolowToUser'] == true", modifier['$set']['seenByFolowToUser'] == true)
			return true;
		}
		return false;
	},
	remove: (userId, doc) => {
		return userId == doc.userId;
	}
});

///publications

Meteor.publish(pubsubNames.subscriptions.subscribed, function(userId) {
	console.log((this.userId || 'anonimus') + ' looking for subsriptions  of ' + userId);
	return Subscriptions.find({ userId: userId });
});

Meteor.publish(pubsubNames.subscriptions.followers, function(userId): IHFSubscription[] | Mongo.Cursor<IHFSubscription> {
	console.log((this.userId || 'anonimus') + ' looking for folowers  to ' + userId);
		return Subscriptions.find({ folowToUserId: userId });
});

Meteor.publish(pubsubNames.subscriptions.followersNewCount, function() {
	var userId = (<Subscription>this).userId;
	console.log('subscribing for new count for userId ', userId)
	if (!userId) {
		return [];
	}
	// console.log('subscribing for new count for userId ', userId )
	// var s = Subscriptions.find().fetch()[0];
	// s.seenByFolowToUser 
	Counts.publish(this, pubsubNames.subscriptions.followersNewCount, Subscriptions.find({
		folowToUserId: userId,
		seenByFolowToUser: false
	}));
});

Meteor.publish(pubsubNames.subscriptions.followersCount, function(userId) {
	if (!userId) {
		return [];
	} 
	Counts.publish(this, pubsubNames.subscriptions.followersCount, Subscriptions.find({
		folowToUserId: userId
	}));
});

Meteor.publish(pubsubNames.subscriptions.subscriptionsCount, function(userId) {
	if (!userId) {
		return [];
	}
	Counts.publish(this, pubsubNames.subscriptions.subscriptionsCount, Subscriptions.find({
		userId: userId
	}));
});

///methods

Meteor.methods({
	[methodNames.subscriptions.imSubscribed]: function(subscrUserId: string) {
		var userId: string = this.userId;
		if (!userId) return false; //все анонимы не подписаны ни на кого.
		console.log(userId + ' interest for subscribing ' + subscrUserId);
		var subscription = Subscriptions.findOne({ userId: userId, folowToUserId: subscrUserId });
		return !!subscription; //привожу существующий объект к bool 
	},
	[methodNames.subscriptions.subscribe]: function(subscribeTo: string) {
		if (!subscribeTo || !this.userId) return '';
		var userId = this.userId;
		console.log(userId + ' subscibing to ' + subscribeTo);

		var subscriptionId = Subscriptions.insert({
			folowToUserId: subscribeTo,
			userId: userId,
			createdAt: new Date(),
			seenByFolowToUser: false
		});
		// console.log('subscribed');
		return subscriptionId;
	},
	[methodNames.subscriptions.unsubscribe]: function(unsubscribeFrom: string) {
		check(unsubscribeFrom, String);
		if (!unsubscribeFrom || !this.userId) return false;
		var userId = this.userId;
		console.log(userId + 'unsubscribing from ' + unsubscribeFrom);

		Subscriptions.remove({
			folowToUserId: unsubscribeFrom,
			userId: userId
		});
		// console.log('unsubscribed');
		return true;
	},
})