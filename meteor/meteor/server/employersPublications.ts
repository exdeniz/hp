/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../interfaces/Employer.d.ts' />
/// <reference path='../lib/EmployersCollection.ts' />
Employers.allow({
	insert: function(userId, doc) {
		return (userId && doc.name ? true : false);
    }
});

Meteor.publish(pubsubNames.employers.employersById, function(employerIds: string[]): (Mongo.Cursor<IHFEmployer> | IHFEmployer[]) {
	if (employerIds.length == 0) {
		console.log(pubsubNames.employers.employersById , 'employerIds.length == 0');
		return [];
	}
	return Employers.find({
		_id: { $in: employerIds }
	});
})

Meteor.publish(pubsubNames.employers.employerById, function(employerId: string) {
	var employer = Employers.find({
		_id: employerId
	});
	return employer;
})