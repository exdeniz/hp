/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../interfaces/Chat.d.ts' />
/// <reference path='../interfaces/User.d.ts' />
/// <reference path='../lib/PublicationNames.ts' />
/// <reference path='../lib/ChatsCollection.ts' />
/// <reference path='../lib/ImagesCollection.ts' />
/// <reference path='../shared/sharedEnums.ts' />
declare var Fiber: any
Fiber = Npm.require('fibers');

UserCovers.allow({
    insert: function(userId, doc) {
		console.log('insert userId:', userId)
		return (userId ? doc.userId == userId : false);
    },
    remove: function(userId, doc) {
		console.log('remove isUserID?', userId);
		return (userId ? doc.userId == userId : false);
    },
    download: function(userId, fileObj) {
		return true;
		// return true;
    },
    update: function(userId, doc, fieldNames, modifier) {
		// console.log('userId', userId);
		console.log('update userId:', userId)
		return (userId ? doc.userId == userId : false);
    }
});

Meteor.publish(pubsubNames.images.cover.current, function(userId): any {
    var userId = userId || this.userId
	
	if (userId == null) {
		console.log(pubsubNames.images.cover.current, 'userId is null');
		return [];
	}
	console.log(pubsubNames.images.cover.current, 'subscribing for userId',userId);
    return UserCovers.find({ userId : userId }, {limit : 1});
});

Meteor.methods({
	[methodNames.profile.setCurrentCover]: function(coverId, userId) {
		userId = userId || this.userId;
		//TODO добавить проверки
		console.log('remove old cover for user', userId);

		UserCovers.remove({  _id: { $ne: coverId }, userId: userId });
	}
})