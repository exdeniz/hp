/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../interfaces/Institution.d.ts' />
/// <reference path='../lib/InstitutionsCollection.ts' />
/// <reference path='../lib/PublicationNames.ts' />

Institutions.allow({
	insert: function(userId, doc) {
		return (userId && doc.name ? true : false);
    }
});

Meteor.publish(pubsubNames.institutions.institutionById, function(institutionId: string) {
	return Institutions.find({
		_id: institutionId
	});
})