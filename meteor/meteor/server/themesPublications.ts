/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../lib/ThemesCollection.ts' />
/// <reference path='../interfaces/Theme.d.ts' />
/// <reference path='../lib/PublicationNames.ts' />


Themes.allow({
	insert: (userId, doc) => {
		return userId ? true :  false;
	}
});

ThemesSubscriptions.allow({
	insert: (userId, doc) => {
		return userId ? true: false;
	},
	update: (userId, doc, fieldNames, modifier) => {
		return userId ? true: false;
	},
	remove: (userId, doc) => {
		return userId ? true: false;
	}
});

Meteor.publish(pubsubNames.themes.all, function(){
	return Themes.find({});
})

Meteor.publish(pubsubNames.themes.subscriptions, function(){
	var userId = this.userId;
	if(!userId) return;
	return ThemesSubscriptions.find({userId: userId});
})

Meteor.methods({
	[methodNames.themes.create]: function(theme: Theme) {
		if (!theme || !this.userId) return {};
		var userId = this.userId;
		var newTheme: Theme = {
			createdAt: new Date(),
			name: theme.name,
			parentId: theme.parentId,
			rootId: theme.rootId,
			createdBy: userId,
		};

		var themeId = Themes.insert(newTheme);
		newTheme._id = themeId;
		return newTheme;
	},
	[methodNames.themes.subscribe]: function(themeId: string) {
		if (!themeId || !this.userId) return '';
		var userId = this.userId;

		var subscriptionId = ThemesSubscriptions.insert({
			themeId: themeId,
			userId: userId
		});
		return subscriptionId;
	},
	[methodNames.themes.unsubscribe]: function(themeId: string) {
		if (!themeId || !this.userId) return false;
		var userId = this.userId;

		ThemesSubscriptions.remove({
			themeId: themeId,
			userId: userId
		});
		return true;
	},
})