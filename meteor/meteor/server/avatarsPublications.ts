/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../interfaces/Chat.d.ts' />
/// <reference path='../interfaces/User.d.ts' />
/// <reference path='../lib/PublicationNames.ts' />
/// <reference path='../lib/ChatsCollection.ts' />
/// <reference path='../lib/ImagesCollection.ts' />
/// <reference path='../shared/sharedEnums.ts' />
declare var Fiber: any
Fiber = Npm.require('fibers');

UserAvatars.allow({
    insert: function(userId, doc) {
		console.log('insert userId:', userId)
		return (userId ? true : false);
    },
    remove: function(userId, doc) {
		console.log('remove isUserID?', userId);
		return (userId ? true : false);
    },
    download: function(userId, fileObj) {
		return true;
		// return true;
    },
    update: function(userId, doc, fieldNames, modifier) {
		// console.log('userId', userId);
		console.log('update userId:', userId)
		return (userId ? true : false);
    }
});

Meteor.publish(pubsubNames.images.avatars.current, function(userId): any {
    var userId = userId || this.userId
	
	if (userId == null) {
		console.log(pubsubNames.images.avatars.current, 'userId is null');
		return [];
	}
	console.log(pubsubNames.images.avatars.current, 'subscribing for userId',userId);
    return UserAvatars.find(
		{ current: true,
			userId : userId },
		{
			sort: {
				createdAt: -1
			},
			limit: 1
		});
});

Meteor.methods({
	[methodNames.profile.setCurrentAvatar]: function(avatarId, userId) {
		userId = userId || this.userId;
		//TODO добавить проверки
		console.log('updateing current avatar for user', userId);

		UserAvatars.update({ userId: userId, current: true }, { $set: { current: false } }, {
			multi: true
		});
		UserAvatars.update(avatarId, { $set: { current: true } });
	}
})