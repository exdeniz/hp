/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../lib/EventsCollection.ts' />
/// <reference path='../lib/PublicationNames.ts' />
/// <reference path='../lib/common/preparePostToEvent.ts' />
Meteor.methods({
	[methodNames.events.addEvents]: function(post: any) {
		var userId = this.userId;
		if (!userId) {
			throw new Meteor.Error("forbiden");
		}
		// console.log(post);
		
		var event = ConvertPostToEventBeforeSaving(userId, post);
		var content = (<IHFArticleContent>event.content);
		if (!(content.text || content.title || content.filesId)) {
			throw new Meteor.Error("no content");
		}
		var hashtags = ScanTextForHashtags(content.text);
		if(hashtags.length){
			event.tags = hashtags;
		}
		return event;
	}
});