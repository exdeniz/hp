/// <reference path='../typings/tsd.d.ts' />
Meteor.publish(pubsubNames.users.userById, function(userId: string) {
	// console.log('publish user',userId)
	return Meteor.users.find({ _id: userId });
});
Meteor.publish(pubsubNames.users.usersById, function(userIds: string[]): (Mongo.Cursor<IHFUser> | IHFUser[]) {
	console.log(pubsubNames.users.usersById, 'subscribe executing')
	if (userIds.length == 0) {
		console.log(pubsubNames.users.usersById , 'userIds.length == 0');
		return [];
	}
	var usrs = HFUsers.find({
		_id: { $in: userIds }
	});
	console.log(pubsubNames.users.usersById + 'users',usrs.fetch());
	return usrs;
})

Meteor.publish(pubsubNames.users.all, function(term: string, page: number, perPage: number, sort: { createdAt: number }) {
	var self = this;
	// console.log('findObj', findObj['$or'][0].username['$in']);
	var terms = _.map(term.split(/['"`+><&\s,./\\|()-]/igm), (t) => {
		return new RegExp('^' + t, 'igm');
	});
	var findObj: any = {
		$or: [
			{ username: { $in: terms } },
			{ emails: { $elemMatch: { address: { $in: terms } } } },
			{ 'profile.firstName': { $in: terms } },
			{ 'profile.middleName': { $in: terms } },
			{ 'profile.lastName': { $in: terms } },
		]
	};
	if (!term || term.length == 0) {
		findObj = {};
	}
	var limit = page * perPage;
	var handle = (<Mongo.Collection<IHFUser>>Meteor.users).find(findObj, {
		sort: sort,
		limit: limit
	}).observeChanges({
		added: (id, fields) => {
			// console.log('added', id, fields);
			self.added('userSearchResult', id, fields);
		},
		removed: (id) => {
			console.log('removed', id);
			self.removed('userSearchResult', id)
		}
	});
	self.ready();
	self.onStop(() => {
		console.log('stop handle');
		handle.stop();
	});

	// return handle;
})