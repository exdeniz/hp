// /// <reference path='../../typings/meteor/meteor.d.ts' />
// ///  <reference path='../../interfaces/Chat.d.ts' />
// ///  <reference path='../../interfaces/User.d.ts' />
// ///  <reference path='../../lib/ChatsCollection.ts' />
// /**
//  * Chat
//  */
// class Chat {
// 	constructor(public rawChat: IHFChat) {
// 	}
	
// 	/**
// 	 * Может ли пользователь писать в чат
// 	 */
// 	public isInChat(user: IHFUser | string) {
// 		var userId;
// 		if (typeof user === "string") {
// 			userId = user;
// 		} else {
// 			userId == user._id;
// 		}
// 		return (_.findIndex(this.rawChat.userIds, (uId) => { return uId === userId }) > -1);
// 	}
// 	/**
// 	 * getUnreadedMessages
// 	 */
// 	public getUnreadedMessages(userId: string) {
// 		return Messages.find({
// 			chatId: this.rawChat._id,
// 			readedBy: { $ne: userId }
// 		});
// 	}
	
// 	/**
// 	 * isHaveUnreadedMessage
// 	 */
// 	public isHaveUnreadedMessage(userId: string) {
// 		return this.getUnreadedMessages(userId).count() > 0;
// 	}
	
// 	/**
// 	 * Проверяет является ли пользователь админом чата.
// 	*/
// 	public isChatAdmin(userId: string | IHFUser) {
// 		if (typeof userId === "string") {
// 			return this.rawChat.adminId === userId;
// 		}
// 		return this.rawChat.adminId === (<IHFUser>userId)._id;
// 	}
// 	/**
// 	 * Проверяет является ли пользователь создателем чата.
// 	 */
// 	public isChatCreator(user: string | IHFUser) {
// 		if (typeof user === "string") {
// 			return this.rawChat.createdById === user;
// 		}
// 		return this.rawChat.createdById === (<IHFUser>user)._id;
// 	}

// 	public get users() {
// 		return Meteor.users.find({
// 			_id: { $in: this.rawChat.userIds }
// 		});
// 	}

// 	public get status() {
// 		return this.rawChat.status
// 	}


// 	public get value() {
// 		return this.rawChat.statusLastUpdatedAt
// 	}


// 	public get messages() {
// 		var selector = { chatId: this.rawChat._id }
// 		// Messages.findOne().createdAt
// 		return Messages.find(selector, {
// 			sort: { createdAt: -1 }
// 		});
// 	}

// 	public get lastMessage() {
// 		return Messages.findOne(this.rawChat.lastMessageId);
// 	}


// 	public get admin() {
// 		return Meteor.users.findOne(this.rawChat.adminId);
// 	}


// 	public get creator() {
// 		return Meteor.users.findOne(this.rawChat.createdById);
// 	}



// }
// /**
//  * ChatService
//  */
// class ChatService {
// 	constructor() {

// 	}
// }