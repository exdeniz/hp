/// <reference path='../../typings/meteor/meteor.d.ts' />
interface IBuvleAuthOptions {
	isBuvle: boolean;
	access_token: string;
	isCreatingNew?: boolean;
}

Meteor.startup(() => {
	if (!Meteor.settings['buvle_secret']) {
		throw new Meteor.Error("there is no buvle_secret field at the Meteor.settings");
	}

	var bffr = Npm.require('buffer');
	var crpt = Npm.require('crypto');
	class Validation {
		constructor(private Secret: string, private Algoritm = "SHA256") {
			// console.log('Validation obj created');
			// console.log(this);
		}
		// private Buffer: Buffer;
		// private crypto: any;
		/** Validate token and return decoded value */
		isAccessTokenValid(access_token: string): {
			isValid: boolean,
			value: any;
		} {
			var parts = access_token.split('.');
			// this.Buffer
			// console.log('parts = ', parts);
			var value = JSON.parse(bffr.Buffer(parts[1], 'base64'));
			var isValid = crpt.createHmac(this.Algoritm, bffr.Buffer(this.Secret, 'utf8'))
				.update(parts[0] + '.' + parts[1])
				.digest('base64') === parts[2];
			// console.log('isValid == ', isValid);
			// console.log('value == ', value);
			return {
				isValid: isValid,
				value: value
			};
		}
		static Validate(access_token: string, secret: string, algoritm = "SHA256"): boolean {
			// console.log('static Validate(' + access_token + ', ' + secret + ', ' + algoritm + ');');
			return new Validation(secret, algoritm)
				.isAccessTokenValid(access_token)
				.isValid;

		}	
	}

	Accounts['registerLoginHandler']("buvleauth", function(options: IBuvleAuthOptions): any {
		if (!options.isBuvle) {
			return undefined;
		}
		console.log(1);
		
		var validator = new Validation(Meteor.settings['buvle_secret']);
		console.log(2);
		var { value, isValid } = validator.isAccessTokenValid(options.access_token)
		console.log(3);
		if (!isValid) {
			// console.log('not valid throwing err');
			console.log(4);
			throw new Meteor.Error('verifying token error');
		} else {
			console.log(5);
			var emailExists = Meteor.users.findOne({ emails: { $elemMatch: {$eq : email} } })
			console.log(6);
			if (emailExists) {
				console.log(7);
				throw new Meteor.Error("user with same email is already exist");
			}
			console.log(8);
			var userId = value['http://oauth.buvle.com/userid'];
			var email = value.sub;
			console.log(9);
			var exists = Meteor.users.findOne({ _id: userId });
			console.log(10);
			if (exists) {
				console.log(11);
				return {
					userId: exists._id
				};
			} else {
				console.log(12);
				var result = Meteor.users.insert({
					_id: userId,
					username: email,
					emails: [
						{
							address: email,
							verified: false
						}
					],
					createdAt: new Date().getTime(),
					profile: {}
				});
				console.log(13);
				return {
					userId: result
				};
			}
		}
	});
});

