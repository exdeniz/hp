/// <reference path='../typings/tsd.d.ts' />
/// <reference path='../interfaces/Chat.d.ts' />
/// <reference path='../interfaces/User.d.ts' />
/// <reference path='../lib/PublicationNames.ts' />
/// <reference path='../lib/ChatsCollection.ts' />
/// <reference path='../shared/sharedEnums.ts' />
/// <reference path='lib/ChatService.ts' />
Chats.allow({
	update: function(userId, chat, fieldNames) {
		//TODO улучшить безопасность
		return chat.adminId == userId;
	}
});

Meteor.methods({
	[methodNames.chats.sendMessage] : function(
		message : {
			text : string;
			chatId : string;
		}
	) {
		var isValid = true;
		var senderId = this.userId;
		if (!senderId  || !isValid) {
			throw new Meteor.Error('forbidden');
		}
		
		var chat =  Chats.findOne({
			'users.id' : senderId,
			_id : message.chatId
		});
		if(!chat || chat.status != EntityStatuses.Active) {
			// return false;
			throw new Meteor.Error('chat is not exist');
		}
		var createdAt  = new Date();
		var newMessage:IHFMessage = {
			chatId : chat._id,
			createdAt: createdAt,
			userId: senderId,
			content: {
				text: message.text
			},
			readedBy : [],
			messageStatus: MessageStatuses.isSended,
		}
		var usersIdForUpdate = _(chat.users)
			.filter((u) => u.id != senderId)
			.map((u) => u.id);
			
		var newMessageId = Messages.insert(newMessage, 
		(err,id) => {
					Chats.update(
						{ //selector
							_id: chat._id,
							'users.id': { $in: usersIdForUpdate }
						}, { //modifier
							$set: {
								lastMessageAt: createdAt,
								lastMessageId: id,
								'users.$.isHasNewMessages': true
							}
						});
		});
		return newMessageId;
	},
	[methodNames.chats.create]: function(forId: string) {
		var createrId:string = this.userId;
		var chatId;
		if (forId == createrId || !createrId) {
			throw new Meteor.Error("forbidden");
		}
		var chat = Chats.findOne({
			'users.id': {$all :[createrId, forId]},
			 users : { $size : 2}
		 });
		if(!chat){
			chatId = Chats.insert({
				adminId: createrId,
				createdAt: new Date(),
				createdById: createrId,
				users: [{ 	 
							id: forId,
							 isHasNewMessages:true},
						{
							id: createrId,
							isHasNewMessages: false
						}],
				type: ChatTypes.Dialog,
				status: EntityStatuses.Active,
				statusLastUpdatedAt: new Date()
			});	
		}
		else{
			chatId = chat._id;
		}
		return  chatId;
	}
});
Meteor.publish(pubsubNames.chats.myChats, function(): any {
	if (this.userId == null) {
		return [];
	}
	var userId = <string>this.userId;
	var self = this;
	var chats = Chats.find({
		'users.id': userId, //получаем все чаты в которых участвует пользователь
		status: EntityStatuses.Active, // и статус которых "Активен"
		lastMessageId : {$exists : true}
	});
	//console.log('pubsubNames.chats.myChats chats for user',userId, chats.fetch());
	return chats;
});
Meteor.publish(pubsubNames.chats.lastMessages, function(msgIds: string[]): any {
	var userId = this.userId;
	if (!userId) return [];
	var msgs  = Messages.find({
		_id : {$in : msgIds }
	});
	var chatsId = msgs.map(m => m.chatId);
	var chatsCount = Chats.find({
		_id : { $in : chatsId},
		'users.id' : userId
	}).count();
	
	if(msgIds.length != chatsCount){
		return  [];
	}
	return msgs;
});

Meteor.publish(pubsubNames.chats.messages, function(chatId:string):any{
	var userId = this.userId;
	var chat = Chats.findOne({
		'users.id': userId,
		_id: chatId
	});
	if(!chat){
		return [];
	}
	return Messages.find({chatId : chat._id}, {sort: {createdAt: 1}});
});

Meteor.publish(pubsubNames.chats.messagesNewCount,function(){
	var userId:string = this.userId;
	if(!userId){
		throw new Meteor.Error('forbidden');
	}
	Counts.publish(this,pubsubNames.chats.messagesNewCount,Chats.find({
		'users.id' : userId,
		'users.isHasNewMessages' : true,
		status: EntityStatuses.Active, // и статус которых "Активен"
		lastMessageId : {$exists : true}
	}));
});

Meteor.publish(pubsubNames.chats.chatById, function(chatId: string) {
	//users: { $elemMatch: { id: userId }} // check user exist in chat
	return Chats.find({ _id: chatId });
});