/// <reference path='../typings/tsd.d.ts' />
/// <reference path='usersPublication.ts' />
/// <reference path='../lib/UserCollection.ts' />
/// <reference path='chatPublications.ts' />
/// <reference path='../lib/ImagesCollection.ts' />
/// <reference path='avatarsPublications.ts' />
/// <reference path='subscriptionsPublications.ts' />
/// <reference path='coverPublications.ts' />
/// <reference path='employersPublications.ts' />
/// <reference path='institutionsPublications.ts' />
/// <reference path='themesPublications.ts' />
/// <reference path='eventsPublications.ts' />
/// <reference path='lib/buvle.oauth.server.ts' />
// declare var Buvle: any;
Meteor.startup(function() {
	// FS.debug = true;
	// Buvle.Hooks = {
	// 	onActivate: function() {
	// 		console.log('activated');
	// 	}
	// }
	if(Themes.find({}).count() == 0 ){
		var fId = Themes.insert({
			level: 1,
			name: 'Семья',
			childCount: 1
		});
		var jId = Themes.insert({
			level: 1,
			name: 'Работа',
			childCount: 1
		});
		var hId = Themes.insert({
			level: 1,
			name: 'Хобби',
			childCount: 1
		});
		var bId = Themes.insert({
			level: 1,
			name: 'Бизнес',
			childCount: 1
		});
		var iId = Themes.insert({
			level: 1,
			name: 'Я',
			childCount: 1
		});
		Themes.insert({
			level: 2,
			name: 'Дети',
			parentId: fId,
			rootId: fId
		});
		Themes.insert({
			level: 2,
			name: 'Офис',
			parentId: jId,
			rootId: jId
		});
	}
});