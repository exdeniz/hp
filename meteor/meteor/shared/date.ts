/// <reference path='../typings/tsd.d.ts' />\

class DateSharedType {
	public dates: Array<number>;
	public months: Array<any>;
	public years: Array<number>;
	constructor() {
		this.dates = _.range(1, 32);
		this.months = [
			{name:'января', value: 0}, 
			{name:'февраля', value: 1}, 
			{name:'марта', value: 2}, 
			{name:'апреля', value: 3}, 
			{name:'мая', value: 4}, 
			{name:'июня', value: 5}, 
			{name:'июля', value: 6}, 
			{name:'августа', value: 7}, 
			{name:'сентября', value: 8}, 
			{name:'октября', value: 9}, 
			{name:'ноября', value: 10}, 
			{name:'декабря', value: 11}];
		this.years = _.range(1900, new Date().getFullYear() + 1);
	}
}

declare var DateShared: DateSharedType;
DateShared = new DateSharedType(); 