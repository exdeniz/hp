enum Genders {
    /** Мужской */
    Male,                              // Мужской
    /** Женский */
    Female                             // Женский
    // #?ОШ: А что если юзер через некоторое время сменит пол?.. По идее, для юзера надо хранить даты изменения им пола и привязывать к ним размещенные им материалы. Иначе фотка женщины может оказаться подписанной системой женским именем, типа "Разместил Петя 15.12.202015"
}
this.Genders = Genders;
// declare var Gender: {
//     [key: string]: Genders;
// };
// Gender = {
//     Male: Genders.Male,
//     Female: Genders.Female
// }

enum GroupTypes {
    /** Бизнесс-группы */
    Business,                          // Бизнесс-группы
    /** Личная-группа */
    Private,                           // Личная-группа
    /** Хобби-группа */
    Hobby,                             // Хобби-группа
    /** Общественная группа */
    Public,                            // Общественная группа
    /** Семейная группа */
    Family,                            // Семейная группа
    /** Пользовательская группа  */
    UserType                           // Пользовательская группа #?ОШ: Правильно я понял, что это группа, привязанная к пользователю вне зависимости от тематики?
}

this.GroupTypes = GroupTypes;

// declare var GroupType: {
//     [key: string]: GroupTypes;
// };
// GroupType = {
//     Business: GroupTypes.Business,
//     Private: GroupTypes.Private,
//     Hobby: GroupTypes.Hobby,
//     Public: GroupTypes.Public,
//     Family: GroupTypes.Family,
//     UserType: GroupTypes.UserType
// }
// declare var EntityStatuses:EntityStatusesTs
enum EntityStatuses {
    /** Системная сущность, не появляется в интерфейсах*/
    System,                            // Системная сущность, не появляется в интерфейсах
    /** Скрытая сущность */
    Hiden,                             // Скрытая сущность
    /** Активный (нормальный статус сущности) */
    Active,                            // Активный (нормальный статус сущности)
    /** Заблокированный */
    Blocked,                           // Заблокированный
    /** Удаленный */
    Deleted                            // Удаленный
}
this.EntityStatuses = EntityStatuses;

// declare var EntityStatus: {
//     [key: string]: EntityStatuses                 // Удаленный
// }
// EntityStatus = {
//     /** Системная сущность, не появляется в интерфейсах*/
//     System: EntityStatuses.System,                            // Системная сущность, не появляется в интерфейсах
//     /** Скрытая сущность */
//     Hiden: EntityStatuses.Hiden,                              // Скрытая сущность
//     /** Активный (нормальный статус сущности) */
//     Active: EntityStatuses.Active,                            // Активный (нормальный статус сущности)
//     /** Заблокированный */
//     Blocked: EntityStatuses.Blocked,                           // Заблокированный
//     /** Удаленный */
//     Deleted: EntityStatuses.Deleted                            // Удаленный
// }

enum Roles {
    Admin,                             // Администратор
    Expert,                            // Эксперт
    Publisher,                         // Публикатор
    Editor,                            // Редактор
    User                               // Пользователь
}
this.Roles = Roles;
// declare var Role: { [key: string]: Roles };
// Role = {
//     Admin: Roles.Admin,
//     Expert: Roles.Expert,
//     Publisher: Roles.Publisher,
//     Editor: Roles.Editor,
//     User: Roles.User
// }

///вытащил из контратов, но под вопросом, надо ли это сейчас.
enum EventPaymentType {
    Service,			//Услуга реальная, результаты которой могут быть получены только реальными действиями: подвезти, встретить, передать, оформить, защитить и т.д.
    Product,			//Товар реальный, который имеет вес, размер, количество, упаковку, фото, стоимость доставки
    VirtualService,			//Виртуальная услуга, результаты которой могут быть получены виртуально, например: написать статью, обработать фотки, нарезать ролик из видео и т.д
    VirtualProduct			//Виртуальный товар, который может быть скачан в виде файла
}
this.EventPaymentType = EventPaymentType;
enum ChatTypes {
    /**Диалог */
    Dialog,
    /**Чат */
    Chat
}
this.ChatTypes = ChatTypes;
// declare var ChatType: { [key: string]: ChatTypes }
// ChatType = {
//     /**Диалог */
//     Dialog: ChatTypes.Dialog,
//     /**Чат */
//     Chat: ChatTypes.Chat
// }

enum MessageStatuses {
    isSending,
    isSended,
    isReaded
}

this.MessageStatuses = MessageStatuses;
// declare var MessageStatus: { [key: string]: MessageStatuses }
// MessageStatus = {
//     isSending: MessageStatuses.isSending,
//     isSended: MessageStatuses.isSended,
//     isReaded: MessageStatuses.isReaded
// }
enum CropType {
    avatar,
    cover
}
this.CropType = CropType


enum InstitutionTypes {
    school,            // Школа
    highSchool         // ВУЗ
}

this.InstitutionTypes = InstitutionTypes;

enum ContextTypes {
    /** Личный */
    Private = 0,
    /** Семейный */
    Family = 2,
    /** Общественный */
    Public = 1,
    /** Бизнесс */
    Business = 3,
    /** Хобби */
    Hobby = 4
}
this.ContextTypes = ContextTypes;