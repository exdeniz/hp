/// <reference path='../typings/tsd.d.ts' />
//  <reference path='../shared/sharedEnums.ts' />
interface IHFSubscription {
	userId: string,
	folowToUserId: string,
	createdAt : Date,
	seenByFolowToUser : boolean
}