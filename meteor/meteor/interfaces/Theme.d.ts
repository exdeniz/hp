/// <reference path='../shared/sharedEnums.ts' />
/// <reference path='../typings/tsd.d.ts' />
// Описание сущности Темы

interface Theme {
    _id?: string;
    name: string;                   // Название темы
    parentId?: string;                 // Родительская тема
    rootId?: string;                 // Родительская тема
    // children: Theme[];              // Вложенные темы
    level?: number;                 // Уровень вложенности темы
    // photo?: Photo;                   // Фото ассоциирующееся с темой
    createdAt?: Date;                // Время добавления темы
    createdBy?: string;               // Автор темы
    status?: EntityStatuses;           // Статус сущности
    childCount?: number
}


interface UserThemesResult {
	themes: _.Dictionary<Theme>;
	subscriptions: ThemeSubscription[];
}

interface ThemeSubscription {
    userId: string;
	themeId: string;
	includeChildren?: boolean;
}
