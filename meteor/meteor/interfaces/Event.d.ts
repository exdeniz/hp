/// <reference path='../typings/tsd.d.ts' />
/// <reference path="User.d.ts" />
/// <reference path="../shared/sharedEnums.ts" />
// interface IMembership {
// 	userId: string;
// 	createdAt: Date;
// 	isConfirmed: boolean;
// }

interface DateRange {
	from: Date;                     // Время начала периода
	to?: Date;                      // Время конца периода
}

interface IHFEvent extends IStatetable {
	createdAt: Date;
	createdByGroup?: string;
	createdBy?: string;
	linkedEventIds: string[];
	nestedEventIds: string[];
	members: string[];
	context: ContextTypes;
	themeId: string;
	status: EntityStatuses;
	tags: string[];
	content: IHFEventContent;
	dates: DateRange;
	likesCount : number;
	// repostedBy?: string;
	///добавить координаты и текстовый индекс
}

interface IHFEventContent {

}
interface IHFArticleContent extends IHFEventContent {
	title: string;                      // Заголовок
	text: string;                       // Текст
	photoId: string;                       // Заглавное фото
	filesId: string[];              // Список фото в тексте
} 