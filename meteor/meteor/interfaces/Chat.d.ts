/// <reference path='../typings/tsd.d.ts' />
//  <reference path='../shared/sharedEnums.ts' />
interface IHFChat {
	// static Types:{chat : string, dialog : string};
	_id?: string;
	createdById: string;
	adminId: string;
	users: {
		id: string,
		isHasNewMessages: boolean
	}[];
	createdAt: Date;
	status: EntityStatuses;
	statusLastUpdatedAt: Date;
	type: ChatTypes;
	title?: string;
	pictureUrl?: string;
	pictureObjId?: string;
	lastMessageAt?: Date;
	lastMessageId?: string;
}

interface IHFMessage {
	_id?: string;
	chatId: string;
	createdAt: Date;
	userId: string;
	content?: IHFContent;
	messageStatus: MessageStatuses;
	readedBy: IHFUser[]
}

interface IHFContent {
	text: string;
}