/// <reference path='../shared/sharedEnums.ts' />
/// <reference path='../typings/tsd.d.ts' />
// interface User {
//       // uid?: string;                      // Уникальный глобальный идентификатор пользователя
//       // email?: string;                    // Email пользователя
//       profile: UserProfile;              // Профиль пользователя
      
      
//       // cover?: Photo;                     // Обложка страницы пользователя
//       // createdAt: Date;                   // Дата создания пользователя
      
     

//       state?: UserStates;                // Состояние пользователя
//       relations?: UserRelation[];       // Список связей пользователя
// }

interface IHFImageRef {
      url?: string;
      collectionName: string;
      imageId: string;
}

interface IStatetable {
      status?: EntityStatuses;           // Статус сущности
}

interface IHFUser extends Meteor.User {
      profile: IHFUserProfile;
}

// Описание сущности профиля пользователя
interface IHFUserProfile extends IStatetable {
      isVirtual: boolean;                // Флаг, является ли пользователь виртуальным
      // avatar?: Photo;                 // Аватар
      createdBy?: IHFUser;               // Пользователь, создавший нового пользователя (актуален при создании виртуального пользователя)
      roles?: Roles[];                   // Роли пользователя
      // themes?: Theme[];               // Темы на которые подписан пользователь
      firstName: string;                 // Имя //#&ОШ: это список с датами изменения
      lastName: string;                  // Фамилия //#&ОШ: это список с датами изменения
      middleName?: string;               // Отчество //#&ОШ: это список с датами изменения
      madenName?: string                 // девичья фамилия
      bornOn?: Date;                     // Дата рождения
      gender?: Genders;                  // Пол //#&ОШ: это список с датами изменения
      additionalInfo?: string;           // Дополнительное поле "обо мне"
      additionalInfo2?: string;          // Второе дополнительное поле "обо мне", в случае, если пользователь имеет роль эксперта, то здесь хранится описание его как эксперта
      // city?: Point;                   // Город местонахождения пользователя //#&ОШ:Место постоянного проживания, т.к. местоположение мы вроде договорились определять
      address?: {
            text: string;
      };
      avatarId: string;                  // ссылка изображение
      currentLocationName?: string       // Название текущего местоположения
      jobs: IHFUserJob[];                // Карьера пользователя
      geneology: any;
      educations: IHFUserEducation[];    // образование
      contacts: any;
      
}

// Описание сущности работы пользователя
interface IHFUserJob{
      employerId: string;                 // Идентификатор работадателя
      startedAt: number;                  // Год начала работы
      finishedAt?: number,                // Год завершения работы
      now?: boolean,                      // По текущее время
      position: string                    // Должность
}

// Описание сущности этапа обучения
interface IHFUserEducation{
      institutionId: string;              // идентификатор учреждения
      institutionType?: InstitutionTypes   // тип учреждения (школа, вуз ...)
      description: string;                // описание
      startedAt: number;                  // год начала обучения
      finishedAt?: number,                // год завершения обучения
      now?: boolean,                      // по текущее время
}