docker build -t dev-hf-proxy proxy
docker build -t dev-hf-meteor ../meteor

# docker pull registry.intelligent-systems.pw/oauth/api:latest
export PRIMARY_DOMAIN="dev.happy-family.com"
export DEPLOYMENT_PREFIX="dev-hf-"
export AUTH_SECRET='dev-hf-auth-secret';
export ADMIN_EMAIL='rbarinov@gmail.com';
export SENDGRID_FROM="no-reply@barinov.pw";
export SENDGRID_FROMNAME="BARINOV";
export SENDGRID_ACCOUNT="romanbarinov";
export SENDGRID_PASSWORD="ghjnjnbvv";
export SENDGRID_BCC=$ADMIN_EMAIL;
export DB_NAME="production";
export HTTP_PROTOCOL="http";

docker rm -f "$DEPLOYMENT_PREFIX"data-mongo
docker rm -f "$DEPLOYMENT_PREFIX"auth-mongo
docker rm -f "$DEPLOYMENT_PREFIX"auth-api
docker rm -f "$DEPLOYMENT_PREFIX"auth-admin
docker rm -f "$DEPLOYMENT_PREFIX"meteor
docker rm -f "$DEPLOYMENT_PREFIX"proxy

docker run \
 --restart=always \
 -m=128m \
 -d \
 --name "$DEPLOYMENT_PREFIX"auth-mongo \
 -v /datadrive/"$DEPLOYMENT_PREFIX"auth-mongo:/data/db \
 -p 47017:27017 \
 mongo:2.6 --smallfiles --noprealloc

docker run \
 --restart=always \
 -m=128m \
 -d \
 --name "$DEPLOYMENT_PREFIX"data-mongo \
 -v /datadrive/"$DEPLOYMENT_PREFIX"data-mongo:/data/db \
 -p 37017:27017 \
 mongo:2.6 --smallfiles --noprealloc

docker run \
 --restart=always \
 -d \
 --link "$DEPLOYMENT_PREFIX"auth-mongo:mongo \
 --name "$DEPLOYMENT_PREFIX"auth-api \
 -e OAUTH_Issuer=$PRIMARY_DOMAIN \
 -e OAUTH_Audience=$PRIMARY_DOMAIN \
 -e OAUTH_Secret=$AUTH_SECRET \
 -e OAUTH_Admin=$ADMIN_EMAIL \
 -e OAUTH_SocialLogin='true' \
 -e OAUTH_ServiceUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/auth/" \
 -e OAUTH_Domain="localhost,$PRIMARY_DOMAIN,auth.$PRIMARY_DOMAIN,admin.$PRIMARY_DOMAIN" \
 -e OAUTH_Expiration=3600 \
 -e OAUTH_MongoConnectionString=';;auth;mongo:27017' \
 -e OAUTH_SignupHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/signup" \
 -e OAUTH_ActivateHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/activate" \
 -e OAUTH_RestorePasswordHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/restore" \
 -e OAUTH_ResetPasswordHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/reset" \
 -e OAUTH_ChangePasswordHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/changePassword" \
 -e OAUTH_ChangeUserpicHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/changeUserpic" \
 -e OAUTH_ChangeProfileHookUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/api/hooks/changeProfile" \
 -e OAUTH_ActivateUrlPattern="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/auth/user/activate?code={0}" \
 -e OAUTH_ResetUrlPattern="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/reset?login={0}&token={1}" \
 -e OAUTH_WelcomeUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/login" \
 -e OAUTH_LoginUrl="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/login" \
 -e OAUTH_SendgridFrom="$SENDGRID_FROM" \
 -e OAUTH_SendgridFromName="$SENDGRID_FROMNAME" \
 -e OAUTH_SendgridAccount="$SENDGRID_ACCOUNT" \
 -e OAUTH_SendgridPassword="$SENDGRID_PASSWORD" \
 -e OAUTH_SendgridBcc="$SENDGRID_BCC" \
 registry.intelligent-systems.pw/oauth/api:latest

docker run \
 -d \
 --restart=always \
 --name="$DEPLOYMENT_PREFIX"auth-admin \
 -e OAUTH_HOST="$HTTP_PROTOCOL://$PRIMARY_DOMAIN/auth/" \
 registry.intelligent-systems.pw/generic/authadmin

# docker rm -f "$DEPLOYMENT_PREFIX"meteor

docker run -d --restart=always --name="$DEPLOYMENT_PREFIX"meteor \
 --link "$DEPLOYMENT_PREFIX"data-mongo:mongo \
-v /datadrive/dev-hf-uploads:/uploads \
 --env MONGO_URL="mongodb://mongo/happyfamily" \
 --env ROOT_URL="http://$PRIMARY_DOMAIN/" \
 --env METEOR_SETTINGS='{ "buvle_secret": "'"$AUTH_SECRET"'", "public": { "OAUTH_HOST": "'"$HTTP_PROTOCOL://$PRIMARY_DOMAIN/auth/"'" } }' \
 dev-hf-meteor

# docker rm -f "$DEPLOYMENT_PREFIX"proxy
docker run -d --restart=always --name="$DEPLOYMENT_PREFIX"proxy \
  --env FRONT="$DEPLOYMENT_PREFIX""meteor:80" \
  --env AUTH_API="$DEPLOYMENT_PREFIX""auth-api:6003" \
  --env AUTH_ADMIN="$DEPLOYMENT_PREFIX""auth-admin:5002" \
  --link "$DEPLOYMENT_PREFIX"meteor:meteor \
  --link "$DEPLOYMENT_PREFIX"auth-api:auth-api \
  --link "$DEPLOYMENT_PREFIX"auth-admin:auth-admin \
  --env PRIMARY_DOMAIN="$PRIMARY_DOMAIN" \
  -p 8080:80 \
  dev-hf-proxy
