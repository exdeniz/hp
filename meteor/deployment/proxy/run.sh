cp /templates/nginx.tmpl /etc/nginx.conf

# setup PRIMARY_DOMAIN
if [ ! $PRIMARY_DOMAIN ]
then
	echo "Should set env PRIMARY_DOMAIN";
	exit 1;
fi
sed -i 's#$PRIMARY_DOMAIN#'"$PRIMARY_DOMAIN"'#g' /etc/nginx.conf

# setup FRONT_NODES
if [ ! $FRONT ]
then
        echo "Should set env FRONT in format: 'host:port,host-2:port-2";
        exit 1;
fi
FRONT_NODES="";
nodes=$(echo $FRONT | tr "," "\n");
for x in $nodes
do
	FRONT_NODES=$FRONT_NODES" server $x;"
done
sed -i 's#$FRONT_NODES#'"$FRONT_NODES"'#g' /etc/nginx.conf


# setup AUTH_API_NODES
if [ ! $AUTH_API ]
then
        echo "Should set env AUTH_API in format: 'host:port,host-2:port-2";
        exit 1;
fi
AUTH_API_NODES="";
nodes=$(echo $AUTH_API | tr "," "\n");
for x in $nodes
do
	AUTH_API_NODES=$AUTH_API_NODES" server $x;"
done
sed -i 's#$AUTH_API_NODES#'"$AUTH_API_NODES"'#g' /etc/nginx.conf


# setup AUTH_ADMIN_NODES
if [ ! $AUTH_ADMIN ]
then
        echo "Should set env AUTH_ADMIN in format: 'host:port,host-2:port-2";
        exit 1;
fi
AUTH_ADMIN_NODES="";
nodes=$(echo $AUTH_ADMIN | tr "," "\n");
for x in $nodes
do
	AUTH_ADMIN_NODES=$AUTH_ADMIN_NODES" server $x;"
done
sed -i 's#$AUTH_ADMIN_NODES#'"$AUTH_ADMIN_NODES"'#g' /etc/nginx.conf

# run nginx
echo "started nginx";
nginx
