var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cached'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

// Копируем и минимизируем изображения

gulp.task('images', function() {
    gulp.src('./assets/img/**/*')
        .pipe(cache('images'))
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'))
        .pipe(reload({
            stream: true
        }));

});

// gulp.task('icons', function() {
//     gulp.src('./assets/icons/**/*.svg')
//         .pipe(cache('icons'))
//         .pipe(gulp.dest('./public/icons'))
//         .pipe(reload({
//             stream: true
//         }));

// });
