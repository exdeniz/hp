$(".chosen-select").chosen({disable_search_threshold: 10});
$(".chosen-select-search").chosen({no_results_text: "Oops, nothing found!"});

$('.messangerUserScroll').slimScroll({
    height: $('.messangerUsers').height(),
    railVisible: true,
    railColor: '#fff',
    railOpacity: 1,
    color: '#dde1e5',
    size: '12px',
    alwaysVisible: true,
    borderRadius: '0px',
    railBorderRadius : '0px'
});

$('.messangerChatMessagesScroll').slimScroll({
    height: $('.messangerChatMessages').height(),
    railVisible: true,
    railColor: '#fff',
    railOpacity: 1,
    color: '#dde1e5',
    size: '12px',
    alwaysVisible: true,
    borderRadius: '0px',
    railBorderRadius : '0px'
});

$('.icheckRound').iCheck({
    checkboxClass: 'iCheckRound',
    radioClass: 'iCheckRound'
});

$('.icheckBox').iCheck({
    checkboxClass: 'iCheckBox',
    radioClass: 'iCheckBox'
});




$('#datetimepicker').datetimepicker({
    // debug: true,
    locale: 'ru',
    format: 'L'
});


$('.mapObjectScroll').slimScroll({
    height: $('.postAddFormMapObjects').height(),
    railVisible: true,
    railColor: '#fff',
    railOpacity: 1,
    color: '#dde1e5',
    size: '12px',
    alwaysVisible: true,
    borderRadius: '0px',
    railBorderRadius : '0px'
});


$('.mapTooltipScroll').slimScroll({
    height: $('.mapTooltip').height(),
    railVisible: false,
    railColor: '#fff',
    railOpacity: 1,
    color: '#2E576A',
    size: '6px',
    alwaysVisible: true,
    borderRadius: '3px',
    railBorderRadius : '3px'
});

$('.chatContentScroll').slimScroll({
    height: $('.chatContent').height(),
    railVisible: false,
    railColor: '#fff',
    railOpacity: 1,
    color: '#2E576A',
    size: '6px',
    alwaysVisible: true,
    borderRadius: '3px',
    railBorderRadius : '3px'
});
