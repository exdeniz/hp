// var tooltipSlider = document.getElementById('slider-tooltip');
//
// noUiSlider.create(tooltipSlider, {
// 	start: [50],
// 	range: {
// 		'min': 0,
// 		'max': 100
// 	},
//     format: {
// 	  to: function ( value ) {
// 		return Math.round(value)
// 	  },
// 	  from: function ( value ) {
// 		return Math.round(value)
// 	  }
// 	}
// });
//
// var tipHandles = tooltipSlider.getElementsByClassName('noUi-handle'),
// 	tooltips = [];
//
// // Add divs to the slider handles.
// for ( var i = 0; i < tipHandles.length; i++ ){
// 	tooltips[i] = document.createElement('div');
// 	tipHandles[i].appendChild(tooltips[i]);
//     tooltips[i].className += 'sliderTooltip';
// }
//
// // // Add a class for styling
// // tooltips.className += 'tooltip';
// // // Add additional markup
// // tooltips.innerHTML = '<strong>Value: </strong><span></span>';
// // // Replace the tooltip reference with the span we just added
// // tooltips[1] = tooltips[1].getElementsByTagName('span')[0];
//
// // When the slider changes, write the value to the tooltips.
// tooltipSlider.noUiSlider.on('update', function( values, handle ){
// 	tooltips[handle].innerHTML = values[handle];
// });
//
// function sliderVertical() {
//     var tooltipSliderVertical = document.getElementById('slider-tooltip-vertical');
//
//     noUiSlider.create(tooltipSliderVertical, {
//     	start: [25],
//     	range: {
//     		'min': 0,
//     		'max': 50
//     	},
//         orientation: "vertical",
//         format: {
//     	  to: function ( value ) {
//     		return Math.round(value)
//     	  },
//     	  from: function ( value ) {
//     		return Math.round(value)
//     	  }
//     	}
//     });
//
//     var tipHandles = tooltipSliderVertical.getElementsByClassName('noUi-handle'),
//     	tooltips = [];
//
//     // Add divs to the slider handles.
//     for ( var i = 0; i < tipHandles.length; i++ ){
//     	tooltips[i] = document.createElement('div');
//     	tipHandles[i].appendChild(tooltips[i]);
//         tooltips[i].className += 'sliderTooltip';
//     }
//
//     // // Add a class for styling
//     // tooltips.className += 'tooltip';
//     // // Add additional markup
//     // tooltips.innerHTML = '<strong>Value: </strong><span></span>';
//     // // Replace the tooltip reference with the span we just added
//     // tooltips[1] = tooltips[1].getElementsByTagName('span')[0];
//
//     // When the slider changes, write the value to the tooltips.
//     tooltipSliderVertical.noUiSlider.on('update', function( values, handle ){
//     	tooltips[handle].innerHTML = values[handle];
//     });
// }
// sliderVertical()
