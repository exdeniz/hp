app.run(function ($templateCache) {
  $templateCache.put('ngDropdowns/templates/dropdownSelectItem.html', [
    '<li ng-class="{divider: (dropdownSelectItem.divider && !dropdownSelectItem[dropdownItemLabel]), \'divider-label\': (dropdownSelectItem.divider && dropdownSelectItem[dropdownItemLabel])}">',
      '<a href="" class="dropdown-item" ng-class="dropdownSelectItem.class"',
      ' ng-if="!dropdownSelectItem.divider"',
      ' ng-href="{{dropdownSelectItem.href}}"',
      ' ng-click="selectItem()">',
      '<div ng-if="dropdownSelectItem.class" ng-class="dropdownSelectItem.class"></div>',
      '{{dropdownSelectItem[dropdownItemLabel]}}',
      '</a>',
      '<span ng-if="dropdownSelectItem.divider">',

        '{{dropdownSelectItem[dropdownItemLabel]}}',
      '</span>',
    '</li>'
  ].join(''));

  $templateCache.put('ngDropdowns/templates/dropdownSelect.html', [
    '<div class="wrap-dd-select statusDropdown">',
    '<span class="selected statusDropdown"><div ng-class="dropdownModel.class"></div>',
    '<div ng-if="!dropdownModel.class">{{dropdownModel[labelField]}}</div></span>',
    '<ul class="dropdown">',
    '<li ng-repeat="item in dropdownSelect"',
    ' class="dropdown-item"',
    ' dropdown-select-item="item"',
    ' dropdown-item-label="labelField">',
    '</li>',
    '</ul>',
    '</div>'
  ].join(''));
});

app.controller('sidebarCtrl', function($scope, $http, $timeout, $interval) {
    $scope.selectStatus = [{
        class: 'statusDropdownOnline',
        label: 'В сети'
    }, {
        class: 'statusDropdownBusy',
        label: 'Занят'
    }]

    $scope.selectStatusSelected = {
        class: 'statusDropdownOnline',
        label: 'В сети'
    }


    $scope.selectContent = [{
        divider: true,
        label: 'Выберите контекст:'
    },
        {
        value: 'private',
        label: 'Личный'
    }, {
        value: 'family',
        label: 'Семейный '
    }, {
        value: 'bussines',
        label: 'Бизнес'
    },{
        value: 'hobby',
        label: 'Хобби'
    },{
        value: 'all',
        label: 'Общественный'
    }]

    $scope.selectContentSelected = {
        value: 'private',
        label: 'Личный'
    }

})
