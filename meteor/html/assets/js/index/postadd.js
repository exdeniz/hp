app.controller('postAddCtrl', function($scope, $http, $timeout, $interval) {

    $scope.visibleSelectOptions = [
           {
               text: 'Доступно всем',
               value: 'default'
           },
           {
               text: 'Доступно мне',
               value: 'another value',
               someprop: 'somevalue'
           },
           {
               // Any divider option with a 'text' property will
               // behave similarly to a divider and cannot be selected.
               text: 'Доступно подписчикам',
               value: 'another value',
               someprop: 'somevalue'
           },
           {
               // Example of an option with the 'href' property
               text: 'Доступно семье',
               value: 'another value',
               someprop: 'somevalue'
           }
       ];
       $scope.visibleSelectOption = {
           text: "Доступно всем"
       }

    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;

    $scope.setInputFocus = function (){
      $scope.$broadcast('UiSelectDemo1');
    };

    $scope.enable = function() {
      $scope.disabled = false;
    };

    $scope.disable = function() {
      $scope.disabled = true;
    };

    $scope.enableSearch = function() {
      $scope.searchEnabled = true;
    };

    $scope.disableSearch = function() {
      $scope.searchEnabled = false;
    };

    $scope.availableNames = ['Igor Ivanov','Sergey Isaev','Burger Kong','Sasha Greys']

    $scope.userChoise = {}
    $scope.userChoise.name = ['Burger Kong','Sasha Greys']

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];

    $scope.singleDemo = {};
    $scope.singleDemo.color = 'Red';
    $scope.multipleDemo = {};
    $scope.multipleDemo.colors = ['Blue','Red'];
    $scope.multipleDemo.colors2 = ['Blue','Red'];
    
})

$('#addPostStartDate').datetimepicker({
    // debug: true,
    locale: 'ru',
    format: 'Do MMMM YYYY'
});

$('#addPostStartEnd').datetimepicker({
    // debug: true,
    locale: 'ru',
    format: 'Do MMMM YYYY'
})
