$(".js-show-news").on("click", function() {
    $(".newsOverlay").toggleClass('newsOverlayShow')
    $("html").toggleClass("has-open-news");
    return false;
})
$(".js-hide-news").on("click", function() {
    newsHide();
    return false;
})

var body = $("body");


body.swipe({swipeUp:newsShow, allowPageScroll:"vertical"})
$('.news').swipe({swipeDown:newsHide, allowPageScroll:"vertical",threshold:150})

function newsShow(event, direction, distance, duration, fingerCount) {
    $(".newsOverlay").addClass('newsOverlayShow')
    $(".js-up-news").addClass('newsButtonUpShow');
    $("html").addClass("has-open-news");
}
function newsHide(event, direction, distance, duration, fingerCount) {
    $("html").removeClass("has-open-news");
    $('.news').scrollTo(0,0)
    $(".newsOverlay").removeClass('newsOverlayShow')
    $(".js-up-news").removeClass('newsButtonUpShow');
}


docReady(function() {

    var grid = document.querySelector('.newsItems');
    var msnry = new Masonry(grid, {
         columnWidth: '.newsItem',
         itemSelector: '.newsItem',
         "gutter": '.newsItemGutter',
         isFitWidth: true
    });



    jQuery(function($) {
        $('.news').bind('scroll', function() {
            if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
                    var elems = [];
                    var fragment = document.createDocumentFragment();
                    for (var i = 0; i < 3; i++) {
                        var elem = getItemElement();
                        fragment.appendChild(elem);
                        elems.push(elem);
                    }
                    // append elements to container
                    grid.appendChild(fragment);
                    // add and lay out newly appended elements
                    msnry.appended(elems);
            }
        })
    });

});

// create <div class="grid-item"></div>
function getItemElement() {
    var elem = document.createElement('div');
    elem.className = 'newsItem';
    elem.innerHTML = '<div class="newsItemImage"><img src="img/newsImage-1.jpg" alt=""></div><div class="newsItemText">#hundp #agentur</div><div class="newsItemIconFB"></div>'
    return elem;
}

$(window).bind('DOMMouseScroll mousewheel', function(event) {
    var delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
    if (delta >= 0) {
        if($('.news').scrollTop() == 0) {
            newsHide();
        }
    }
    else {
        newsShow();
    }
});

$('.js-up-news').click(function () {
    $('.news').scrollTo(0,500, { easing:'swing' } );
    newsHide();
})
