$('.bannerItems').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    lazyLoad: 'progressive',
    mobileFirst: true
});

$(".js-show-nav").on("click", function() {
    $("html").toggleClass("has-open-nav");
    return false;
});


var timer;

$(window).on('mousemove', function() {
    var hovered = $(".slick-next:hover, .slick-prev:hover").length;

    $('.slick-next').addClass('slick-arrowShow');
    $('.slick-prev').addClass('slick-arrowShow');
    try {
        clearTimeout(timer);
    } catch (e) {}
    // $(".slick-next, .slick-prev").mouseout(function() {
    if (!hovered) {
        timer = setTimeout(function() {
            $('.slick-next').removeClass('slick-arrowShow');
            $('.slick-prev').removeClass('slick-arrowShow');
        }, 500);
    }

});
$('.bannerMarkenItems').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false
});


$(function(){
    if (location.hash.indexOf('slide') + 1) {
        var slide = Number(location.hash.replace(/\D+/g,""));
        $('.bannerMarkenItems').slick('slickGoTo', slide - 1);
        $('.bannerItems').slick('slickGoTo', slide - 1);    }
});

window.onhashchange = function () {
    if (location.hash.indexOf('slide') + 1) {
        var slide = Number(location.hash.replace(/\D+/g,""));
        $('.bannerMarkenItems').slick('slickGoTo', slide - 1);
        $('.bannerItems').slick('slickGoTo', slide - 1);    }
}

$('.videoSlide').vide({
  mp4: './video/westpark_spot_v3_website_1.mp4',
  ogv: './video/westpark_spot_v3_website_1.ogv',
  webm: './video/westpark_spot_v3_website_1.webm'
});

// $(window).on('orientationchange', function(e) {
//      window.location.reload();
// });

viewportUnitsBuggyfill.init();
